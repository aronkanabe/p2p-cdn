package org.aronkanabe.p2p_library.models

enum class DataLifecycle {
    STARTED,
    PAUSED,
    RESUMED,
    DOWNLOADED,
    DELETED,
    UNKNOWN
}