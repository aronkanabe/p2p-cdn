package org.aronkanabe.p2p_library.repositories.file

import org.aronkanabe.p2p_library.models.TorrentFileDetailed
import org.aronkanabe.p2p_library.models.TorrentFile
import java.io.File
import java.util.*

class FileMemorySource {

    fun get(name: String): File? {
        return null
    }

    fun cache(torrentFileListItemList: List<TorrentFile>) {
    }

    fun cache(guid: UUID, torrentFileDetailed: TorrentFileDetailed) {
    }
}