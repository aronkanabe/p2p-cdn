package org.aronkanabe.p2p_library.datasource

import org.aronkanabe.p2p_library.models.TorrentFileDetailed
import org.aronkanabe.p2p_library.models.TorrentFile
import java.util.*

interface TrackerDataProvider {
    suspend fun getTorrentFileList(): List<TorrentFile>
    suspend fun getTorrentFile(guid: UUID): TorrentFileDetailed?
}