package org.aronkanabe.p2p_library.repositories.torrentfile

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.aronkanabe.p2p_library.datasource.database.Database
import org.aronkanabe.p2p_library.datasource.database.entities.toTorrentFile
import org.aronkanabe.p2p_library.datasource.database.entities.toTorrentFileDb
import org.aronkanabe.p2p_library.datasource.database.entities.toTorrentFileDetailed
import org.aronkanabe.p2p_library.datasource.database.entities.toTorrentFileDetailedDb
import org.aronkanabe.p2p_library.models.TorrentFileDetailed
import org.aronkanabe.p2p_library.models.TorrentFile
import java.util.*

class TorrentFileLocalSource {

    suspend fun get(): List<TorrentFile> =
        withContext(Dispatchers.IO) {
            val db = Database.db!!
            return@withContext db.torrentFileDao().select().map { it.toTorrentFile() }
        }

    suspend fun get(guid: UUID): TorrentFileDetailed? =
        withContext(Dispatchers.IO) {
        val db = Database.db!!
        return@withContext db.torrentFileDetailedDao().select(guid)?.toTorrentFileDetailed()
    }

    suspend fun cache(torrentFiles: List<TorrentFile>) {
        withContext(Dispatchers.IO) {
            val db = Database.db!!
            db.torrentFileDao().insert(torrentFiles.map { it.toTorrentFileDb() })
        }
    }

    suspend fun cache(guid: UUID, torrentFileDetailed: TorrentFileDetailed) {
        withContext(Dispatchers.IO) {
            val db = Database.db!!
            db.torrentFileDetailedDao().insert(torrentFileDetailed.toTorrentFileDetailedDb())
        }
    }
}