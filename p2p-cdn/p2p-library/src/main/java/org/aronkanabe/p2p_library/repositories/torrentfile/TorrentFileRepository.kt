package org.aronkanabe.p2p_library.repositories.torrentfile

import org.aronkanabe.p2p_library.models.TorrentFileDetailed
import org.aronkanabe.p2p_library.models.TorrentFile
import java.util.*

object TorrentFileRepository {
    private val localSource: TorrentFileLocalSource = TorrentFileLocalSource()
    private val remoteSource: TorrentFileRemoteSource = TorrentFileRemoteSource()

    suspend fun getTorrentFileListItemList(): List<TorrentFile> {
        val list = localSource.get()
        return list.ifEmpty {
            remoteSource.get().also { localSource.cache(it) }
        }
    }

    suspend fun getTorrentFileById(guid: UUID): TorrentFileDetailed? {
        return  localSource.get(guid) ?: remoteSource.get(guid).also {
            if (it != null) {
                localSource.cache(guid, it)
            }
        }
    }

}