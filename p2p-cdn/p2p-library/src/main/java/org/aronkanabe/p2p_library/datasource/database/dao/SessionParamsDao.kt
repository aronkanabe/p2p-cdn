package org.aronkanabe.p2p_library.datasource.database.dao

import androidx.room.*
import org.aronkanabe.p2p_library.datasource.database.entities.SessionParams

@Dao
interface SessionParamsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(sessionParams: SessionParams)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(sessionParams: SessionParams)

    @Query("SELECT * FROM SessionParams Limit 1")
    fun select(): SessionParams
}