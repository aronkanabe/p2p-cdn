package org.aronkanabe.p2p_library.models

import java.util.*

data class TorrentFile(
    val guid: UUID,
    val tags: Map<String, String>,
    val originalName: String,
)
