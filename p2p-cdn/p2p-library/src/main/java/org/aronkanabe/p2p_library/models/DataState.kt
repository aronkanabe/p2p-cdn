package org.aronkanabe.p2p_library.models

enum class DataState {
    REMOTE,
    DOWNLOADING,
    DOWNLOADED,
    UPLOADING,
    UNKNOWN
}