package org.aronkanabe.p2p_library.datasource.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class TorrentFile(
    @PrimaryKey
    val guid: UUID,
    @ColumnInfo(name = "original_name")
    val originalName: String)


fun org.aronkanabe.p2p_library.models.TorrentFile.toTorrentFileDb(): TorrentFile = TorrentFile(
    guid = guid,
    originalName = originalName
)

fun TorrentFile.toTorrentFile(): org.aronkanabe.p2p_library.models.TorrentFile = org.aronkanabe.p2p_library.models.TorrentFile(
    guid = guid,
    originalName = originalName,
    tags = emptyMap()
)