package org.aronkanabe.p2p_library.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import org.aronkanabe.p2p_library.models.TorrentFileDetailed
import java.util.*

@JsonClass(generateAdapter = true)
data class TorrentFileDetailedDTO(
    @Json(name = "guid")
    val guid: String?,
//    @Json(name = "tags")
//    val tags: List<Map<String, String>>?,
    @Json(name = "content")
    val content: String?,
    @Json(name = "originalName")
    val originalName: String?,
)

fun TorrentFileDetailedDTO.toTorrentFile(): TorrentFileDetailed = TorrentFileDetailed(
    guid = UUID.fromString(guid),
    tags = emptyMap(),
    content = content ?: "",
    originalName = originalName ?: ""
)