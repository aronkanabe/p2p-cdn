package org.aronkanabe.p2p_library.datasource.services

import org.aronkanabe.p2p_library.dto.TorrentFileDTO
import org.aronkanabe.p2p_library.dto.TorrentFileDetailedDTO
import org.aronkanabe.p2p_library.dto.TorrentFileListDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface TorrentFileService {
    @GET("torrentfiles")
    suspend fun getTorrentFiles(): Response<List<TorrentFileDTO>>

    @GET("torrentfiles/{guid}")
    suspend fun getTorrentFile(@Path("guid") guid: String): Response<TorrentFileDetailedDTO>
}