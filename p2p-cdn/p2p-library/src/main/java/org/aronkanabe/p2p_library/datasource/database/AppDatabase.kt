package org.aronkanabe.p2p_library.datasource.database

import androidx.room.Database
import androidx.room.RoomDatabase
import org.aronkanabe.p2p_library.datasource.database.dao.SessionParamsDao
import org.aronkanabe.p2p_library.datasource.database.dao.TorrentFileDao
import org.aronkanabe.p2p_library.datasource.database.dao.TorrentFileDetailedDao
import org.aronkanabe.p2p_library.datasource.database.entities.SessionParams
import org.aronkanabe.p2p_library.datasource.database.entities.TorrentFile
import org.aronkanabe.p2p_library.datasource.database.entities.TorrentFileDetailed

@Database(entities = [SessionParams::class, TorrentFileDetailed::class, TorrentFile::class], version = 1)
abstract class AppDatabase: RoomDatabase() {
    abstract fun sessionParamsDao(): SessionParamsDao
    abstract fun torrentFileDetailedDao(): TorrentFileDetailedDao
    abstract fun torrentFileDao(): TorrentFileDao
}