package org.aronkanabe.p2p_library.models

import java.util.*

data class TorrentFileDetailed(
    val guid: UUID,
    val tags: Map<String, String>,
    val content: String,
    val originalName: String
)
