package org.aronkanabe.p2p_library.datasource.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class SessionParams(
    @PrimaryKey val id: Int = 0,
    @ColumnInfo(name = "session_params")val sessionParams: ByteArray
    ) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SessionParams

        if (id != other.id) return false
        if (!sessionParams.contentEquals(other.sessionParams)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + sessionParams.contentHashCode()
        return result
    }
}