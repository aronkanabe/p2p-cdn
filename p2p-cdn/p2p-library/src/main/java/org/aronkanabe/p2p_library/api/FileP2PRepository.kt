package org.aronkanabe.p2p_library.api

import android.content.Context
import android.util.Base64
import org.aronkanabe.p2p_library.interfaces.FileP2PRepository
import org.aronkanabe.p2p_library.models.DataLifecycle
import org.aronkanabe.p2p_library.models.P2PFile
import org.aronkanabe.p2p_library.repositories.file.FileRepository
import org.aronkanabe.p2p_library.repositories.torrentfile.TorrentFileRepository
import org.aronkanabe.p2p_library.services.TorrentService
import java.io.InputStream
import java.util.*

object FileP2PRepository :
    FileP2PRepository {

    private val p2pFileMap: MutableMap<UUID, P2PFile> = mutableMapOf()

    override suspend fun startService() {
        TorrentService.start()
    }

    override suspend fun stopService() {
        TorrentService.stop()
    }

    override fun getAll(): List<P2PFile> {
        return p2pFileMap.values.toList()
    }

    override suspend fun download(context: Context, guid: UUID): P2PFile {
        val torrentFile = TorrentFileRepository.getTorrentFileById(guid)
            ?: throw IllegalArgumentException("Torrent file(guid: $guid) not found")
        // TODO: Save torrentFile metadata as Tags and original name
        val file = FileRepository.createTorrentFile(
            context,
            base64StringToStream(torrentFile.content),
            torrentFile.guid
        )

        TorrentService.download(file, guid, FileRepository.getBaseDirectory(context))
        val p2pFile = P2PFile(guid, torrentFile.originalName)
        p2pFile.dataLifecycleObserver.add {
            when (it) {
                DataLifecycle.DELETED -> {
                    p2pFileMap.remove(guid)
                }
                else -> {
                    // Do nothing
                }
            }
        }

        p2pFileMap[guid] = p2pFile
        return p2pFile
    }

    private fun base64StringToStream(base64String: String): InputStream {
        val bytes = Base64.decode(base64String, Base64.DEFAULT)
        return bytes.inputStream()
    }

    override fun findById(guid: UUID): P2PFile? {
        return p2pFileMap[guid]
    }
}