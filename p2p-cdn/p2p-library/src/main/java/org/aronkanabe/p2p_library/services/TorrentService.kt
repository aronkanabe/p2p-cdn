package org.aronkanabe.p2p_library.services

import android.util.Log
import com.frostwire.jlibtorrent.*
import com.frostwire.jlibtorrent.alerts.*
import com.frostwire.jlibtorrent.swig.add_torrent_params
import com.frostwire.jlibtorrent.swig.settings_pack
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.aronkanabe.p2p_library.datasource.database.Database
import org.aronkanabe.p2p_library.repositories.file.FileRepository
import java.io.ByteArrayInputStream
import java.io.File
import java.io.InputStream
import java.util.*

object TorrentService {
    private var sessionManager: SessionManager = SessionManager()

    var torrentHandleMap: MutableMap<UUID, TorrentHandle> = mutableMapOf()

    var torrentInfoMap: MutableMap<Sha1Hash, UUID> = mutableMapOf()

    lateinit var alertListener: AlertListener

    suspend fun start() {
        withContext(Dispatchers.IO) {
            val settingsPack = SettingsPack()
            settingsPack.setBoolean(
                settings_pack.bool_types.allow_multiple_connections_per_ip.swigValue(),
                true
            )
            settingsPack.setBoolean(settings_pack.bool_types.enable_natpmp.swigValue(), true)
            settingsPack.setBoolean(settings_pack.bool_types.enable_upnp.swigValue(), true)
            settingsPack.setBoolean(
                settings_pack.bool_types.report_web_seed_downloads.swigValue(),
                true
            )
            settingsPack.setBoolean(
                settings_pack.bool_types.enable_outgoing_utp.swigValue(),
                false
            )

            alertListener = object : AlertListener {
                override fun types(): IntArray? {
                    return null
                }

                override fun alert(alert: Alert<*>?) {

                    when (val type = alert?.type()) {
                        AlertType.ADD_TORRENT -> {
                            Log.i(
                                "TorrentService",
                                "Type: " + type.toString() + " " + alert.message()
                            )
                            (alert as AddTorrentAlert).handle().resume()
                        }
                        AlertType.BLOCK_FINISHED -> {
                            val blockFinishedAlert = alert as BlockFinishedAlert
                            var progress =
                                (blockFinishedAlert.handle().status().progress() * 100).toInt()
                            Log.i(
                                "TorrentService",
                                "Progress: " + progress + " for torrent name: " + blockFinishedAlert.torrentName()
                            )
                            Log.i(
                                "TorrentService",
                                "Piece index: " + blockFinishedAlert.pieceIndex().toString()
                            )
                        }
                        AlertType.TORRENT_FINISHED -> {
                            Log.i(
                                "TorrentService",
                                "TORRENT_FINISHED : " + (alert as TorrentFinishedAlert).torrentName()
                            )
                        }

                        AlertType.TORRENT_REMOVED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.TORRENT_DELETED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.TORRENT_PAUSED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.TORRENT_RESUMED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.TORRENT_CHECKED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.TORRENT_ERROR -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.TORRENT_NEED_CERT -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.INCOMING_CONNECTION -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.SAVE_RESUME_DATA -> {
                            Log.d(
                                "TorrentService",
                                "Type: " + type.toString() + " " + alert.message()
                            )
                            launch(Dispatchers.IO) {
                                saveResumeData(alert as SaveResumeDataAlert)
                            }
                        }
                        AlertType.FASTRESUME_REJECTED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.METADATA_RECEIVED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.METADATA_FAILED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.FILE_COMPLETED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.FILE_RENAMED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.FILE_RENAME_FAILED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.FILE_ERROR -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.HASH_FAILED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.PORTMAP -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.PORTMAP_ERROR -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.PORTMAP_LOG -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.TRACKER_ANNOUNCE -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.TRACKER_REPLY -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.TRACKER_WARNING -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.TRACKER_ERROR -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.READ_PIECE -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.STATE_CHANGED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
//                    AlertType.DHT_REPLY -> Log.d("TorrentService", "Type: " + type.toString() + " " + alert.message())
//                    AlertType.DHT_BOOTSTRAP -> Log.d("TorrentService", "Type: " + type.toString() + " " + alert.message())
//                    AlertType.DHT_GET_PEERS -> Log.d("TorrentService", "Type: " + type.toString() + " " + alert.message())
                        AlertType.EXTERNAL_IP -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.LISTEN_SUCCEEDED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.STATE_UPDATE -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.SESSION_STATS -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.SCRAPE_REPLY -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.SCRAPE_FAILED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.LSD_PEER -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.PEER_BLOCKED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.PERFORMANCE -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.PIECE_FINISHED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.SAVE_RESUME_DATA_FAILED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.STATS -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.STORAGE_MOVED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.TORRENT_DELETE_FAILED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.URL_SEED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.INVALID_REQUEST -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.LISTEN_FAILED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.PEER_BAN -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.PEER_CONNECT -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.PEER_DISCONNECTED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.PEER_ERROR -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.PEER_SNUBBED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.PEER_UNSNUBBED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.REQUEST_DROPPED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.UDP_ERROR -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.BLOCK_DOWNLOADING -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.BLOCK_TIMEOUT -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.CACHE_FLUSHED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
//                    AlertType.DHT_ANNOUNCE -> Log.d("TorrentService", "Type: " + type.toString() + " " + alert.message())
                        AlertType.STORAGE_MOVED_FAILED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.TRACKERID -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.UNWANTED_BLOCK -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
//                    AlertType.DHT_ERROR -> Log.d("TorrentService", "Type: " + type.toString() + " " + alert.message())
//                    AlertType.DHT_PUT -> Log.d("TorrentService", "Type: " + type.toString() + " " + alert.message())
//                    AlertType.DHT_MUTABLE_ITEM -> Log.d("TorrentService", "Type: " + type.toString() + " " + alert.message())
//                    AlertType.DHT_IMMUTABLE_ITEM -> Log.d("TorrentService", "Type: " + type.toString() + " " + alert.message())
                        AlertType.I2P -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
//                    AlertType.DHT_OUTGOING_GET_PEERS -> Log.d("TorrentService", "Type: " + type.toString() + " " + alert.message())
                        AlertType.LOG -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.TORRENT_LOG -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.PEER_LOG -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.LSD_ERROR -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
//                    AlertType.DHT_STATS -> Log.d("TorrentService", "Type: " + type.toString() + " " + alert.message())
                        AlertType.INCOMING_REQUEST -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
//                    AlertType.DHT_LOG -> Log.d("TorrentService", "Type: " + type.toString() + " " + alert.message())
//                    AlertType.DHT_PKT -> Log.d("TorrentService", "Type: " + type.toString() + " " + alert.message())
//                    AlertType.DHT_GET_PEERS_REPLY -> Log.d("TorrentService", "Type: " + type.toString() + " " + alert.message())
//                    AlertType.DHT_DIRECT_RESPONSE -> Log.d("TorrentService", "Type: " + type.toString() + " " + alert.message())
                        AlertType.PICKER_LOG -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.SESSION_ERROR -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
//                    AlertType.DHT_LIVE_NODES -> Log.d("TorrentService", "Type: " + type.toString() + " " + alert.message())
                        AlertType.SESSION_STATS_HEADER -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
//                    AlertType.DHT_SAMPLE_INFOHASHES -> Log.d("TorrentService", "Type: " + type.toString() + " " + alert.message())
                        AlertType.BLOCK_UPLOADED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.ALERTS_DROPPED -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.SOCKS5_ALERT -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                        AlertType.UNKNOWN -> Log.d(
                            "TorrentService",
                            "Type: " + type.toString() + " " + alert.message()
                        )
                    }
                }
            }
            sessionManager.addListener(alertListener)
            sessionManager.applySettings(settingsPack)

            // can't be null because we created initialize it
            val db = Database.db!!
            val sessionParamsDao = db.sessionParamsDao()
            val sessionParams = sessionParamsDao.select()
            if (sessionParams != null) {
                sessionManager.loadState(sessionParams.sessionParams)
            }

            sessionManager.start()
        }
    }

    private suspend fun saveResumeData(saveResumeDataAlert: SaveResumeDataAlert) {
        withContext(Dispatchers.IO) {
            val byteArray = Vectors.byte_vector2bytes(
                add_torrent_params.write_resume_data(
                    saveResumeDataAlert.params().swig()
                ).bencode()
            )
            val inputStream = ByteArrayInputStream(byteArray)
            FileRepository.createResumeFile(
                Common.appContext,
                inputStream,
                torrentInfoMap[saveResumeDataAlert.params().infoHash()]!!
            )
        }
    }

    suspend fun stop() {
        withContext(Dispatchers.IO) {
            val db = Database.db!!
            if (db != null) {
                val sessionParamsDao = db.sessionParamsDao()
                val sessionParams =
                    org.aronkanabe.p2p_library.datasource.database.entities.SessionParams(
                        sessionParams = sessionManager.saveState()
                    )
                sessionParamsDao.insert(sessionParams)
            }

            var torrentHandleSaving = torrentHandleMap.values.count()
            val savingListener = object : AlertListener {
                override fun types(): IntArray? {
                    return null
                }

                override fun alert(alert: Alert<*>?) {

                    when (val type = alert?.type()) {
                        AlertType.SAVE_RESUME_DATA -> {
                            Log.d(
                                "TorrentService",
                                "Type: " + type.toString() + " " + alert.message()
                            )
                            torrentHandleSaving--
                        }
                        AlertType.SAVE_RESUME_DATA_FAILED -> {
                            Log.d(
                                "TorrentService",
                                "Type: " + type.toString() + " " + alert.message()
                            )
                            torrentHandleSaving--
                        }
                        else -> {
                            // Do nothing
                        }
                    }
                }
            }
            sessionManager.addListener(savingListener)
            torrentHandleMap.values.forEach { torrentHandle -> torrentHandle.saveResumeData() }

            while (torrentHandleSaving > 0) {
                delay(10)
            }

            sessionManager.stop()
            sessionManager.removeListener(alertListener)
            sessionManager.removeListener(savingListener)
        }
    }

    suspend fun download(file: File, guid: UUID, savePathDirectory: File, startPuased: Boolean = false) {
        withContext(Dispatchers.IO) {
            val torrentInfo = TorrentInfo(file)
            val resumeFile = FileRepository.getResumeFile(Common.appContext, guid)
            if (resumeFile != null) {
                sessionManager.download(torrentInfo, savePathDirectory, resumeFile, null, null)
            } else {
                sessionManager.download(torrentInfo, savePathDirectory)
            }
            torrentHandleMap[guid] = sessionManager.find(torrentInfo.infoHash())
            torrentInfoMap[torrentInfo.infoHash()] = guid
            if (startPuased) {
                pauseTorrent(guid)
            }
        }
    }

    fun pauseTorrent(guid: UUID) {
        torrentHandleMap[guid]?.pause()
    }

    fun resumeTorrent(guid: UUID) {
        torrentHandleMap[guid]?.resume()
    }

    fun stopTorrent(guid: UUID) {
        pauseTorrent(guid)
        sessionManager.remove(torrentHandleMap[guid]!!)
        torrentHandleMap.remove(guid)
    }

    suspend fun deleteTorrent(guid: UUID, removeData: Boolean = false) {
        withContext(Dispatchers.IO) {
            if (torrentHandleMap[guid] == null) {
                return@withContext
            }
            pauseTorrent(guid)
            sessionManager.remove(torrentHandleMap[guid])
            if (removeData) {
                FileRepository.deleteFile(torrentHandleMap[guid]?.savePath()!!)
                FileRepository.deleteTorrentFile(guid)
            }
            torrentInfoMap[torrentHandleMap[guid]!!.infoHash()] = guid
            torrentHandleMap.remove(guid)
        }
    }

    fun getFilePath(guid: UUID): String {
        return torrentHandleMap[guid]!!.savePath()
    }

    fun getTorrentStatus(guid: UUID): TorrentStatus? {
        if (torrentHandleMap[guid] == null) {
            return null
        }
        return torrentHandleMap[guid]!!.status()
    }

    fun getTorrentProgress(guid: UUID): Float {
        return getTorrentStatus(guid)?.progress() ?: 0f
    }
}