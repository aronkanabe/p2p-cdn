package org.aronkanabe.p2p_library.repositories.torrentfile

import android.util.Log
import org.aronkanabe.p2p_library.datasource.RestTrackerDataProvider
import org.aronkanabe.p2p_library.datasource.TrackerDataProvider
import org.aronkanabe.p2p_library.models.TorrentFileDetailed
import org.aronkanabe.p2p_library.models.TorrentFile
import java.util.*

class TorrentFileRemoteSource {
    private val trackerFileDataProvider: TrackerDataProvider = RestTrackerDataProvider

    suspend fun get(): List<TorrentFile> {
        var torrentFileListItemList: List<TorrentFile> = emptyList()
        try {
            torrentFileListItemList = trackerFileDataProvider.getTorrentFileList()
        } catch (e: Exception) {
            Log.e("retrofit", e.message.orEmpty())
        }
        return torrentFileListItemList
    }

    suspend fun get(guid: UUID): TorrentFileDetailed? {
        val torrentFileDetailed: TorrentFileDetailed?
        try {
            torrentFileDetailed = trackerFileDataProvider.getTorrentFile(guid)
        } catch (e: Exception) {
            Log.e("retrofit", e.message.orEmpty())
            throw e
        }
        return torrentFileDetailed
    }
}