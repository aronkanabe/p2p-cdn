package org.aronkanabe.p2p_library.datasource.database.dao

import androidx.room.*
import org.aronkanabe.p2p_library.datasource.database.entities.TorrentFile
import org.aronkanabe.p2p_library.datasource.database.entities.TorrentFileDetailed
import java.util.*

@Dao
interface TorrentFileDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(torrentFile: TorrentFile)

    fun insert(torrentFiles: List<TorrentFile>) {
        torrentFiles.forEach { insert(it) }
    }

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(torrentFile: TorrentFile)

    @Query("SELECT * FROM TorrentFile")
    fun select(): List<TorrentFile>
}