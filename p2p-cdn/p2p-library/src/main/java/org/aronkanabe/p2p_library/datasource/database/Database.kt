package org.aronkanabe.p2p_library.datasource.database

import androidx.room.Room
import androidx.room.RoomDatabase
import org.aronkanabe.p2p_library.services.Common
import org.aronkanabe.p2p_library.services.ContextProvider

object Database {
    var db: AppDatabase? = null
        get() {
            if (field != null) return field
            return Room.databaseBuilder(Common.appContext, AppDatabase::class.java, "p2p_library_database").build()
        }
}