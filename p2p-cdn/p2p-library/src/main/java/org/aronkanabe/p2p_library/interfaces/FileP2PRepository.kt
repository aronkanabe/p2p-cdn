package org.aronkanabe.p2p_library.interfaces

import org.aronkanabe.p2p_library.models.P2PFile
import java.io.File

interface FileP2PRepository: P2PRepository<P2PFile> {
}