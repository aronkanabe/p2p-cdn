package org.aronkanabe.p2p_library.datasource.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.aronkanabe.p2p_library.dto.TorrentFileDTO
import org.aronkanabe.p2p_library.models.TorrentFile
import java.util.*

@Entity
data class TorrentFileDetailed(
    @PrimaryKey
    val guid: UUID,
    @ColumnInfo(name = "content")
    val content: String,
    @ColumnInfo(name = "original_name")
    val originalName: String)


fun org.aronkanabe.p2p_library.models.TorrentFileDetailed.toTorrentFileDetailedDb(): TorrentFileDetailed = TorrentFileDetailed(
    guid = guid,
    content = content,
    originalName = originalName
)

fun TorrentFileDetailed.toTorrentFileDetailed(): org.aronkanabe.p2p_library.models.TorrentFileDetailed = org.aronkanabe.p2p_library.models.TorrentFileDetailed(
    guid = guid,
    content = content,
    originalName = originalName,
    tags = emptyMap()
)