package org.aronkanabe.p2p_library.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import org.aronkanabe.p2p_library.models.TorrentFile
import java.util.*

@JsonClass(generateAdapter = true)
data class TorrentFileDTO(
    @Json(name = "guid")
    val guid: String?,
    @Json(name = "tags")
    val tags: Map<String, String>?,
    @Json(name = "originalName")
    val originalName : String?,
)

fun TorrentFileDTO.toTorrentFileListItem(): TorrentFile = TorrentFile(
    guid = UUID.fromString(guid),
    tags = emptyMap(),
    originalName = originalName ?: ""
)