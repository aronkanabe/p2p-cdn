package org.aronkanabe.p2p_library.interfaces

import android.content.Context
import androidx.lifecycle.LiveData
import com.frostwire.jlibtorrent.TorrentStatus
import org.aronkanabe.p2p_library.models.DataState
import org.aronkanabe.p2p_library.models.P2PFile
import java.util.*

interface P2PRepository<T> {
    suspend fun startService()
    suspend fun stopService()
    fun getAll(): List<T>
    suspend fun download(context: Context, guid: UUID): T
    fun findById(guid: UUID): P2PFile?
}