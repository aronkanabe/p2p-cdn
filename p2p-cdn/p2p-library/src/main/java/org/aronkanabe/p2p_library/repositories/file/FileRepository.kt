package org.aronkanabe.p2p_library.repositories.file

import android.content.Context
import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import java.io.InputStream
import java.nio.file.Files
import java.util.*

object FileRepository {
    private val memorySource: FileMemorySource = FileMemorySource()

    suspend fun createTorrentFile(context: Context, content: InputStream, guid: UUID): File =
        createFile(context, content, getTorrentFileName(guid))

    suspend fun createResumeFile(context: Context, content: InputStream, guid: UUID): File =
        createFile(context, content, getResumeFileName(guid))

    suspend fun createFile(context: Context, content: InputStream, fileName: String): File =
        withContext(Dispatchers.IO) {
            var file = memorySource.get(fileName)
            if (file != null) return@withContext file

            file = File(context.applicationContext.filesDir, fileName)
            if (file.exists()) return@withContext file
            content.use { input ->
                file.outputStream().use { output ->
                    input.copyTo(output)
                }
            }
            Log.v("FileRepository", "File created: ${file.absoluteFile}")
            return@withContext file
        }

    suspend fun getResumeFile(context: Context, guid: UUID): File? =
        getFile(context, getResumeFileName(guid))

    suspend fun getTorrentFile(context: Context, guid: UUID): File? =
        getFile(context, getTorrentFileName(guid))

    suspend fun getFile(context: Context, fileName: String): File? =
        withContext(Dispatchers.IO) {
            var file = memorySource.get(fileName)
            if (file != null) return@withContext file

            file = File(context.applicationContext.filesDir, fileName)
            if (file.exists()) return@withContext file
            return@withContext null
        }

    private fun getTorrentFileName(guid: UUID): String = "${guid}.torrent"

    private fun getResumeFileName(guid: UUID): String = "${guid}_resume.dat"

    fun getDirectory(directoryPath: String): File {
        if (!Files.exists(File(directoryPath).toPath())) {
            File(directoryPath).mkdirs()
        }
        return File(directoryPath)
    }

    // create function delete torrentfile by guid
    suspend fun deleteTorrentFile(guid: UUID) {
        withContext(Dispatchers.IO) {
            val torrentFile = File(getTorrentFileName(guid))
            deleteFile(torrentFile.absolutePath)
        }
    }

    suspend fun deleteFile(filePath: String) {
        withContext(Dispatchers.IO) {
            val file = File(filePath)
            if (file.exists()) {
                file.delete()
            }
        }
    }

    fun getBaseDirectory(context: Context): File = context.applicationContext.filesDir
}