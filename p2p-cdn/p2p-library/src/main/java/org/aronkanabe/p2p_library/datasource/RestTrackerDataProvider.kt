package org.aronkanabe.p2p_library.datasource

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.aronkanabe.p2p_library.datasource.services.TorrentFileService
import org.aronkanabe.p2p_library.dto.toTorrentFile
import org.aronkanabe.p2p_library.dto.toTorrentFileListItem
import org.aronkanabe.p2p_library.models.TorrentFileDetailed
import org.aronkanabe.p2p_library.models.TorrentFile
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*

object RestTrackerDataProvider : TrackerDataProvider {
    const val baseUrl = "http://p2p-api.loca.lt/api/"
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
    private val torrentFileService: TorrentFileService =
        retrofit.create(TorrentFileService::class.java)

    override suspend fun getTorrentFileList(): List<TorrentFile> =
        withContext(Dispatchers.Main) {
            val response = torrentFileService.getTorrentFiles()
            if (response.isSuccessful) {
                return@withContext response.body()?.map { it.toTorrentFileListItem() }
                    ?: emptyList()
            }
            return@withContext emptyList()
        }

    override suspend fun getTorrentFile(guid: UUID): TorrentFileDetailed? {
//        withContext(Dispatchers.Main) {
            val response = torrentFileService.getTorrentFile(guid.toString())
            if (response.isSuccessful) {
                return response.body()?.toTorrentFile()
            }
            return null
        }
}