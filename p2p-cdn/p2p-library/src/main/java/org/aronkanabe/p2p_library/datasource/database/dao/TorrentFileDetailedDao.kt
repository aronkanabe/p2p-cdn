package org.aronkanabe.p2p_library.datasource.database.dao

import androidx.room.*
import org.aronkanabe.p2p_library.datasource.database.entities.TorrentFileDetailed
import java.util.*

@Dao
interface TorrentFileDetailedDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(torrentFileDetailed: TorrentFileDetailed)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(torrentFileDetailed: TorrentFileDetailed)

    @Query("SELECT * FROM TorrentFileDetailed WHERE guid = :guid")
    fun select(guid: UUID): TorrentFileDetailed?
}