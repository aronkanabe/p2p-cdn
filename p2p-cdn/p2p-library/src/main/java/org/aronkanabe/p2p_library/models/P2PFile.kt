package org.aronkanabe.p2p_library.models

import androidx.lifecycle.MutableLiveData
import com.frostwire.jlibtorrent.TorrentHandle
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.aronkanabe.p2p_library.api.FileP2PRepository
import org.aronkanabe.p2p_library.repositories.torrentfile.TorrentFileRepository
import org.aronkanabe.p2p_library.services.TorrentService
import java.io.File
import java.lang.NullPointerException
import java.util.*
import kotlin.properties.Delegates

class P2PFile(
    val guid: UUID,
    val originalName: String,
) {
    val dataLifecycleObserver = mutableListOf<(DataLifecycle) -> Unit>()

    val dataLifecycle: DataLifecycle by Delegates.observable(DataLifecycle.UNKNOWN) { _, _, newValue ->
        dataLifecycleObserver.forEach { it(newValue) }
    }

    fun file(): File {
        return File(TorrentService.getFilePath(guid))
    }

    fun pause() {
        TorrentService.pauseTorrent(guid)
    }

    fun resume() {
        TorrentService.resumeTorrent(guid)
    }

    fun stop() {
        TorrentService.stopTorrent(guid)
    }

    suspend fun delete() {
        withContext(Dispatchers.IO) {
            TorrentService.stopTorrent(guid)
            TorrentService.deleteTorrent(guid)
        }
    }

    fun progress(): Float {
        return TorrentService.getTorrentProgress(guid)
    }

    suspend fun state(): DataState {
        if (!TorrentService.torrentHandleMap.containsKey(guid)) {
            try {
                TorrentFileRepository.getTorrentFileById(guid)
            } catch (e: NullPointerException) {
                return DataState.UNKNOWN
            }
            return DataState.REMOTE
        }
        val status =  TorrentService.getTorrentStatus(guid)!!

        if (status.isSeeding) {
            return DataState.UPLOADING
        }

        if (status.isFinished) {
            return DataState.DOWNLOADED
        }

        return DataState.DOWNLOADING
    }
}