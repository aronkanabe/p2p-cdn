package org.aronkanabe.p2p_library.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import org.aronkanabe.p2p_library.models.TorrentFile
import java.util.*

@JsonClass(generateAdapter = true)
data class TorrentFileListDTO(
    @Json(name = "value")
    val torrentFiles: List<TorrentFileDTO>
) {
    fun toTorrentFileList(): List<TorrentFile> {
        val torrentFileList = ArrayList<TorrentFile>()
        torrentFiles.forEach {
            torrentFileList.add(it.toTorrentFileListItem())
        }
        return torrentFileList
    }
}