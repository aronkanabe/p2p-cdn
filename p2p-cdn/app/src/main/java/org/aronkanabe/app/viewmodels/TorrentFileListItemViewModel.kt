package org.aronkanabe.app.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.aronkanabe.p2p_library.models.TorrentFile
import org.aronkanabe.p2p_library.repositories.torrentfile.TorrentFileRepository

class TorrentFileListItemViewModel : ViewModel() {
    var torrentFileList: MutableLiveData<List<TorrentFile>> = MutableLiveData()

    init {
        loadTorrentFileList()
    }

    private fun loadTorrentFileList() {
        viewModelScope.launch {
            torrentFileList.value = TorrentFileRepository.getTorrentFileListItemList()!!
        }
    }
}