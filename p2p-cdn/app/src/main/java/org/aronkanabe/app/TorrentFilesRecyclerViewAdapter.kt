package org.aronkanabe.app

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import org.aronkanabe.P2PCDNLibrary.databinding.FragmentTorrentfilesBinding

import org.aronkanabe.p2p_library.models.TorrentFile

/**
 * [RecyclerView.Adapter] that can display a TorrentFileListItem.
 * TODO: Replace the implementation with code for your data type.
 */
class TorrentFilesListAdapter : ListAdapter<TorrentFile, TorrentFileListItemViewHolder>(diffUtil) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TorrentFileListItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = FragmentTorrentfilesBinding.inflate(inflater, parent, false)
        return TorrentFileListItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TorrentFileListItemViewHolder, position: Int) {
        val item = getItem(position)
        val binding = holder.binding

        binding.GUIDText.text = item.guid.toString()
        binding.OriginalNameText.text = item.originalName

        binding.cardView.setOnClickListener {
            val action = TorrentFilesFragmentDirections.actionTorrentFilesFragmentToTorrentFileDetailsFragment(item.guid.toString())
            Navigation.findNavController(binding.root).navigate(action)
        }
    }
    companion object {
        private val diffUtil = object : DiffUtil.ItemCallback<TorrentFile>() {
            override fun areItemsTheSame(oldItem: TorrentFile, newItem: TorrentFile): Boolean {
                return oldItem.guid == newItem.guid
            }

            override fun areContentsTheSame(oldItem: TorrentFile, newItem: TorrentFile): Boolean {
                return oldItem == newItem
            }
        }
    }


}