package org.aronkanabe.app.viewmodels

import androidx.lifecycle.*
import kotlinx.coroutines.launch
import org.aronkanabe.p2p_library.api.FileP2PRepository
import org.aronkanabe.p2p_library.models.DataState
import org.aronkanabe.p2p_library.models.TorrentFileDetailed
import org.aronkanabe.p2p_library.repositories.torrentfile.TorrentFileRepository
import java.util.*

class TorrentFileDetailedViewModel(
    private val guid: UUID) : ViewModel() {

    var torrentFileDetailed: LiveData<TorrentFileDetailed> = liveData {
        TorrentFileRepository.getTorrentFileById(guid)?.let { emit(it) }
    }

    val p2pFile = FileP2PRepository.findById(guid)
    var progress: MutableLiveData<Float> = MutableLiveData(p2pFile?.progress())

    fun getProgress() = viewModelScope.launch {
//        while (!setOf(DataState.DOWNLOADED, DataState.UPLOADING).contains(p2pFile?.state() ?: DataState.UNKNOWN)) {
//            progress.value = p2pFile?.progress() ?: 0f
//        }
        progress.value = 100f
    }
}