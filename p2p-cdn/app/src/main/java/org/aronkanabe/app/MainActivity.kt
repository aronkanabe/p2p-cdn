package org.aronkanabe.app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.aronkanabe.P2PCDNLibrary.R
import org.aronkanabe.P2PCDNLibrary.databinding.ActivityMainBinding
import org.aronkanabe.p2p_library.api.FileP2PRepository
import org.aronkanabe.p2p_library.services.TorrentService

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)
        val scope = CoroutineScope(Dispatchers.IO)
        scope.launch {
            FileP2PRepository.startService()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        val scope = CoroutineScope(Dispatchers.IO)
        scope.launch {
            FileP2PRepository.stopService()
        }
    }
}