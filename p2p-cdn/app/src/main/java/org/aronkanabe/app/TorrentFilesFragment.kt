package org.aronkanabe.app

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import org.aronkanabe.P2PCDNLibrary.databinding.FragmentTorrentfileListBinding
import org.aronkanabe.app.viewmodels.TorrentFileListItemViewModel


/**
 * A fragment representing a list of Items.
 */
class TorrentFilesFragment : Fragment() {

    private lateinit var binding: FragmentTorrentfileListBinding

    private val torrentFileListItemViewHolder: TorrentFileListItemViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTorrentfileListBinding.inflate(inflater, container, false)

        val listAdapter = TorrentFilesListAdapter()
        binding.recyleView.adapter = listAdapter
        binding.recyleView.layoutManager = LinearLayoutManager(context)

        torrentFileListItemViewHolder.torrentFileList.observe(viewLifecycleOwner) {
            listAdapter.submitList(it)
        }

        return binding.root
    }
}