package org.aronkanabe.app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import kotlinx.coroutines.launch
import org.aronkanabe.P2PCDNLibrary.R
import org.aronkanabe.P2PCDNLibrary.databinding.FragmentTorrentfileDetailsBinding
import org.aronkanabe.app.viewmodels.TorrentFileDetailedViewModel
import org.aronkanabe.app.viewmodels.TorrentFileDetailedViewModelFactory
import org.aronkanabe.p2p_library.api.FileP2PRepository
import java.util.*
import kotlin.math.roundToInt

class TorrentFileDetailsFragment : Fragment() {
    private lateinit var binding: FragmentTorrentfileDetailsBinding
    private lateinit var viewModel: TorrentFileDetailedViewModel
    private val args: TorrentFileDetailsFragmentArgs by navArgs()
    private lateinit var torrentFileId: UUID

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTorrentfileDetailsBinding.inflate(inflater, container, false)
        val toolbar = binding.toolbar
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener {
            Navigation.findNavController(binding.root)
                .navigate(R.id.action_torrentFileDetailsFragment_to_torrentFilesFragment)
        }

        torrentFileId = UUID.fromString(args.torrentFileId)

        binding.downloadButton.setOnClickListener {
            lifecycleScope.launch{
                try {
                    val file = FileP2PRepository.download(requireContext(), torrentFileId)
                } catch (e: IllegalArgumentException) {
                    Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG).show()
                }
            }
        }

        binding.stopButton.setOnClickListener {
            lifecycleScope.launch{
                FileP2PRepository.findById(torrentFileId)?.stop()
            }
        }

        viewModel = ViewModelProvider(this,TorrentFileDetailedViewModelFactory(torrentFileId))[TorrentFileDetailedViewModel::class.java]

        viewModel.torrentFileDetailed.observe(viewLifecycleOwner) { torrentFile ->
            binding.torrentFileOriginalName.text = torrentFile.originalName
            binding.torrentFileId.text = torrentFile.guid.toString()
        }

        viewModel.progress.observe(viewLifecycleOwner) { progress ->
            binding.progressBar.progress = (progress * 100).roundToInt()
        }
        viewModel.getProgress()

        return binding.root
    }


}