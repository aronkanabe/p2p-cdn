package org.aronkanabe.app

import androidx.recyclerview.widget.RecyclerView
import org.aronkanabe.P2PCDNLibrary.databinding.FragmentTorrentfilesBinding

class TorrentFileListItemViewHolder(val binding: FragmentTorrentfilesBinding) : RecyclerView.ViewHolder(binding.root) {
}