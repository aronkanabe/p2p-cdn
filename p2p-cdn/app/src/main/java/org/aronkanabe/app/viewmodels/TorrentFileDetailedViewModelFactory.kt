package org.aronkanabe.app.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.util.*

class TorrentFileDetailedViewModelFactory(private val guid: UUID) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return TorrentFileDetailedViewModel(guid) as T
    }
}