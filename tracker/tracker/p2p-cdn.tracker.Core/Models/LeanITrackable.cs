﻿using MonoTorrent;
using MonoTorrent.Tracker;

namespace p2p_cdn.tracker.Core.Models;

public class LeanITrackable : ITrackable
{
    public LeanITrackable(Torrent torrent)
    {
        InfoHash = torrent.InfoHash;
        Name = torrent.Name;
        Files = torrent.Files.Select(file => file as ITorrentFile).ToList();
    }

    public InfoHash InfoHash { get; }
    public string Name { get; }
    public IList<ITorrentFile> Files { get; }
}