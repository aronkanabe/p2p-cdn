﻿using System.Net;

namespace p2p_cdn.tracker.Core.Configurations
{
    public class TorrentOptions
    {
        public const string Torrent = "Torrent";

        public List<string> Trackers { get; set; }
        public string TorrentFilePath { get; set; }
        
        public IPAddress TrackerAddress { get; set; }
        
        public int TrackerPort { get; set; }

        public TorrentOptions()
        {
            Trackers = new List<string>();
            TrackerAddress = IPAddress.Any;
            TrackerPort = 6888;
            TorrentFilePath = string.Empty;
        }
    }
}