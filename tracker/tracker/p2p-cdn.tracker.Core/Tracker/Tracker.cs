﻿using System.Net;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MonoTorrent;
using MonoTorrent.TorrentWatcher;
using MonoTorrent.Tracker;
using MonoTorrent.Tracker.Listeners;
using p2p_cdn.tracker.Core.Configurations;
using p2p_cdn.tracker.Core.Models;

namespace p2p_cdn.tracker.Core.Tracker;

public class Tracker : BackgroundService
{
    private readonly ILogger<Tracker> logger;
    private readonly TrackerServer trackerServer;
    private readonly TorrentFolderWatcher torrentFolderWatcher;
    private readonly TorrentOptions torrentOptions;

    private readonly string httpUrl;
    private readonly IPEndPoint udpEndPoint;
    private readonly List<ITrackerListener> trackerListeners;
    private TorrentFolderWatcher watcher;

    public Tracker(IOptions<TorrentOptions> torrentOptions, ILogger<Tracker> logger)
    {
        this.logger = logger;
        this.torrentOptions = torrentOptions.Value;
        trackerServer = new TrackerServer();
        // trackerServer.AllowUnregisteredTorrents = true;

        httpUrl = $"http://*:{this.torrentOptions.TrackerPort}/announce/";
        udpEndPoint = new IPEndPoint(this.torrentOptions.TrackerAddress, this.torrentOptions.TrackerPort + 1);

        logger.LogInformation("Tracker listening for HTTP requests on: {HttpEndPoint}", httpUrl);
        logger.LogInformation("Tracker listening for UDP requests on: {UdpEndPoint}", udpEndPoint);

        trackerListeners = new List<ITrackerListener>
        {
            TrackerListenerFactory.CreateHttp(httpUrl),
            TrackerListenerFactory.CreateUdp(udpEndPoint)
        };

        trackerListeners.ForEach(trackerServer.RegisterListener);
        trackerListeners.ForEach(trackerListener => trackerListener.Start());

        SetupTorrentWatcher();
    }

    private void SetupTorrentWatcher()
    {
        watcher = new TorrentFolderWatcher(Path.GetFullPath(torrentOptions.TorrentFilePath), "*.torrent");
        watcher.TorrentFound += delegate(object sender, TorrentWatcherEventArgs e)
        {
            try
            {
                // This is a hack to work around the issue where a file triggers the event
                // before it has finished copying. As the filesystem still has an exclusive lock
                // on the file, monotorrent can't access the file and throws an exception.
                // The best way to handle this depends on the actual application. 
                // Generally the solution is: Wait a few hundred milliseconds
                // then try load the file.
                System.Threading.Thread.Sleep(500);

                var t = Torrent.Load(e.TorrentPath);

                // There is also a predefined 'InfoHashTrackable' MonoTorrent.Tracker which
                // just stores the infohash and name of the torrent. This is all that the tracker
                // needs to run. So if you want an ITrackable that "just works", then use InfoHashTrackable.

                // ITrackable trackable = new InfoHashTrackable(t);
                ITrackable trackable = new LeanITrackable(t);

                // The lock is here because the TorrentFound event is asyncronous and I have
                // to ensure that only 1 thread access the tracker at the same time.
                lock (trackerServer)
                    trackerServer.Add(trackable);
            }
            catch (Exception ex)
            {
                logger.LogError("Error loading torrent from disk: {Message}", ex.Message);
                logger.LogError("Stacktrace: {StackStrace}", ex.ToString());
            }
        };

        watcher.Start();
        watcher.ForceScan();
    }

    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            return Task.Delay(1000, stoppingToken);
        }
        
        return Task.CompletedTask;
    }
}