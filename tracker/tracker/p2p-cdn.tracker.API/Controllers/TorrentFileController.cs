﻿using Microsoft.AspNetCore.Mvc;

namespace p2p_cdn.tracker.API.Controllers
{
    [ApiController]
    [Route("/api/[controller]s")]
    public class TorrentFileController : ControllerBase
    {

        public TorrentFileController()
        {
        }

        // [HttpGet]
        // [ProducesResponseType(StatusCodes.Status200OK)]
        // [ProducesResponseType(StatusCodes.Status400BadRequest)]
        // [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        // [ProducesResponseType(StatusCodes.Status403Forbidden)]
        // [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        // public async Task<ActionResult<IEnumerable<TorrentFileDto>>> FindAllTorrentFiles(CancellationToken cancellationToken = default)
        // {
        //     IEnumerable<TorrentFileDto> torrentFiles;
        //     
        //     try
        //     {
        //         torrentFiles = (await torrentFileService.FindAllAsync(cancellationToken))
        //             .Select(mapper.Map<TorrentFileDto>);
        //     }
        //     catch (TaskCanceledException e)
        //     {
        //         return StatusCode(406);
        //     }
        //     catch (ParameterException e)
        //     {
        //         return BadRequest();
        //     }
        //     catch (Exception e)
        //     {
        //         return new StatusCodeResult(StatusCodes.Status500InternalServerError);
        //     }
        //
        //     return Ok(torrentFiles);
        // }
    }
}
