﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace p2p_cdn.api.API.DTO
{
    public class TorrentFileDto
    {
        public string Guid { get; set; }

        public IDictionary<string, string> Tags { get; set; }
    }
}
