﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace p2p_cdn.tracker.API
{
    public delegate void OnExternalServiceInject(HostBuilderContext context, IServiceCollection services);
}
