using System.Net;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using p2p_cdn.tracker.API;
using Microsoft.Extensions.Configuration;
using p2p_cdn.tracker.Core.Configurations;
using p2p_cdn.tracker.Core.Tracker;

namespace p2p_cdn.tracker.Startup
{
    class Program
    {
        static void Main(string[] args)
        {
            var apiMain = new APIMain(args);
            apiMain.Run((context, services) =>
            {
                services.Configure<TorrentOptions>((torrentOptions) =>
                {
                    var torrentSection = context.Configuration.GetSection(TorrentOptions.Torrent);
                    torrentSection.Bind(torrentOptions);
                    torrentOptions.TrackerAddress = IPAddress.Parse(torrentSection.GetSection("Tracker").GetSection("Address").Value!);
                });
                services.AddHostedService<Tracker>();
            });
        }
    }
}
