﻿using AutoMapper;
using NSubstitute;
using p2p_cdn.api.Persistence;
using p2p_cdn.api.Persistence.DAL;
using p2p_cdn.api.Persistence.Repositories;

namespace p2p_cdn.api.IntegrationTest.Persistence.Repositories
{
    public class TorrentFileRepositoryTests: BaseRepositoryTests<Core.Models.TorrentFile, TorrentFileDAL>
    {
        public TorrentFileRepositoryTests()
        {
        }

        public override BaseRepository<Core.Models.TorrentFile, TorrentFileDAL> GetRepository(IMongoDbContext dbContext, IMapper mapper)
        {
            return Substitute.ForPartsOf<TorrentFileRepository>(dbContext, mapper);
        }
    }
}