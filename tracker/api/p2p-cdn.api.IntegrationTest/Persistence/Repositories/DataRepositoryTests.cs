﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentAssertions;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using p2p_cdn.api.Core.Models;
using p2p_cdn.api.Persistence;
using p2p_cdn.api.Persistence.DAL;
using p2p_cdn.api.Persistence.Repositories;
using Xunit;

namespace p2p_cdn.api.IntegrationTest.Persistence.Repositories
{
    public class DataRepositoryTests : BaseRepositoryTests<Data, DataDAL>
    {
        public override BaseRepository<Data, DataDAL> GetRepository(IMongoDbContext dbContext, IMapper mapper)
        {
            return Substitute.ForPartsOf<DataRepository>(dbContext, mapper);
        }

        public override async Task CreateAsync_WithNotNullParameter_ShouldInsertAsyncCalled()
        {
            var expectedEntity = new Data()
            {
                OriginalName = "testData"
            };

            var baseRepository = GetRepository(dbContext, mapper);

            var entity = await baseRepository.CreateAsync(expectedEntity);
            var entities = (await (await dbContext.GridFsBucket.FindAsync(FilterDefinition<GridFSFileInfo>.Empty))
                    .ToListAsync())
                .Select(gridFsFileInfo => gridFsFileInfo.Id.ToString())
                .ToList();
            entities.Count.Should().Be(1);
            entities.First().Should().BeEquivalentTo(entity.Id);
        }

        public override async Task CreateAsync_WithMongoException_ShouldRollBackAndThrow()
        {
            var session = Substitute.For<IClientSessionHandle>();
            var dbContextMock = Substitute.For<IMongoDbContext>();
            var mongoClientMock = Substitute.For<IMongoClient>();
            var mongoCollectionMock = Substitute.For<IMongoCollection<DataDAL>>();
            dbContextMock.GetCollection<DataDAL>(typeof(DataDAL).Name).Returns(mongoCollectionMock);
            mongoClientMock.StartSession().Returns(session);

            dbContextMock.MongoClient.Returns(mongoClientMock);
            dbContextMock.GetCollection<DataDAL>(typeof(DataDAL).Name).Returns(mongoCollectionMock);
            dbContextMock.GridFsBucket.UploadFromStreamAsync(default, default)
                .ThrowsForAnyArgs(new MongoException("Mongo error"));

            var entity = new Data()
            {
                OriginalName = "testData"
            };

            var baseRepository = GetRepository(dbContextMock, mapper);
            baseRepository.When(b => b.RollBackAsync()).DoNotCallBase();

            await Assert.ThrowsAsync<MongoException>(() => baseRepository.CreateAsync(entity));
            await baseRepository.Received(1).RollBackAsync();
        }

        public override async Task CreateAsync_WithNotNullParameter_ShouldReturnNewId()
        {
            var expectedEntity = new Data()
            {
                OriginalName = "testData"
            };

            var baseRepository = GetRepository(dbContext, mapper);

            var entity = await baseRepository.CreateAsync(expectedEntity);

            var entities = (await (await dbContext.GridFsBucket.FindAsync(FilterDefinition<GridFSFileInfo>.Empty))
                    .ToListAsync())
                .Select(gridFsFileInfo => gridFsFileInfo)
                .ToList();
            entities.First().Id.Should().BeEquivalentTo(ObjectId.Parse(entity.Id));
            entities.First().Filename.Should().BeEquivalentTo(entity.Guid.ToString());
        }

        public override async Task DeleteAsync_WithNotNullParameter_ShouldDeleteAsyncCalled()
        {
            var entityDal = mapper.Map<Data, DataDAL>(new Data());
            entityDal.Base64Content = "";
            var content = System.Convert.FromBase64String(entityDal.Base64Content);

            entityDal.Id =
                await dbContext.GridFsBucket.UploadFromStreamAsync(entityDal.Guid, new MemoryStream(content));
            var baseRepository = GetRepository(dbContext, mapper);
            baseRepository.When(b => b.RollBackAsync()).DoNotCallBase();

            await baseRepository.DeleteAsync(entityDal.Id.ToString());
            var entities = (await (await dbContext.GridFsBucket.FindAsync(Builders<GridFSFileInfo<ObjectId>>.Filter.Eq("_id", entityDal.Id)))
                    .ToListAsync())
                .Select(gridFsFileInfo => gridFsFileInfo)
                .ToList();
            entities.Count.Should().Be(0);
        }

        public override async Task DeleteAsync_WithMongoException_ShouldRollBackAndThrow()
        {
            var session = Substitute.For<IClientSessionHandle>();
            var dbContextMock = Substitute.For<IMongoDbContext>();
            var mongoClientMock = Substitute.For<IMongoClient>();
            var mongoCollectionMock = Substitute.For<IMongoCollection<DataDAL>>();
            dbContextMock.GetCollection<DataDAL>(typeof(DataDAL).Name).Returns(mongoCollectionMock);
            mongoClientMock.StartSession().Returns(session);

            dbContextMock.MongoClient.Returns(mongoClientMock);
            dbContextMock.GetCollection<DataDAL>(typeof(DataDAL).Name).Returns(mongoCollectionMock);
            dbContextMock.GridFsBucket.DeleteAsync(default).ThrowsForAnyArgs(new MongoException("Mongo error"));

            var baseRepository = GetRepository(dbContextMock, mapper);
            baseRepository.When(b => b.RollBackAsync()).DoNotCallBase();

            await Assert.ThrowsAsync<MongoException>(() => baseRepository.DeleteAsync(ObjectId.Empty.ToString()));
            await baseRepository.Received(1).RollBackAsync();
        }
        
        [Fact(Skip="method don't exists in DataRepository context")]
        public override Task GetAllAsync_ShouldCallFindAsync()
        {
            return Task.CompletedTask;
        }

        public override async Task GetByIdAsync_WithNotNullParameter_ShouldFindAsyncCalled()
        {
            var expectedEntity = new Data
            {
                OriginalName = "testData",
                Guid = Guid.NewGuid(),
                Content = new MemoryStream(Encoding.ASCII.GetBytes("testData"))
            };
            var baseRepository = GetRepository(dbContext, mapper);
            var entityCreated = await baseRepository.CreateAsync(expectedEntity);
            entityCreated.Content.Position = 0;
            
            var entity = await baseRepository.GetByIdAsync(expectedEntity.Guid.ToString());
            
            entity.OriginalName.Should().BeEquivalentTo(entityCreated.OriginalName);
            entity.Guid.Should().Be(entityCreated.Guid);
            
            
            var entityMemoryStream = new MemoryStream();
            var entityCreatedMemoryStream = new MemoryStream();
            await entity.Content.CopyToAsync(entityMemoryStream);
            await entityCreated.Content.CopyToAsync(entityCreatedMemoryStream);
            entityMemoryStream.ToArray().Should().BeEquivalentTo(entityCreatedMemoryStream.ToArray());
        }
    }
}