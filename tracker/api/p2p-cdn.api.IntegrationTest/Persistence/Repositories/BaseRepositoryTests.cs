﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentAssertions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Memory;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;
using NeoSmart.StreamCompare;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using p2p_cdn.api.Core.Models;
using p2p_cdn.api.Persistence;
using p2p_cdn.api.Persistence.DAL;
using p2p_cdn.api.Persistence.Mappings;
using p2p_cdn.api.Persistence.Repositories;
using Xunit;
using MappingProfile = p2p_cdn.api.Persistence.Mappings.MappingProfile;

namespace p2p_cdn.api.IntegrationTest.Persistence.Repositories
{
    [Collection("IntegrationTests")]
    public abstract class BaseRepositoryTests<T, TDAL>: IDisposable where T : BaseModel, new() where TDAL : BaseDAL, new()
    {
        protected readonly IMongoDbContext dbContext;
        protected readonly IMongoClient mongoClient;
        protected readonly string mongoUrl;
        protected readonly IMapper mapper;
        protected readonly string databaseName;
        protected readonly ILogger<MongoDbContext> logger;
        protected readonly IConfiguration configuration;
        protected readonly CancellationTokenSource cancellationTokenSource;
        protected readonly StreamCompare streamCompare;

        public BaseRepositoryTests()
        {
            mongoUrl = "mongodb://mongo1:27017/?replicaSet=my-replica-set";
            databaseName = "TestDb";
            mongoClient = new MongoClient(mongoUrl);
            logger = new LoggerFactory().CreateLogger<MongoDbContext>();
            configuration = new ConfigurationManager()
                .Add<MemoryConfigurationSource>((source) =>
                {
                    source.InitialData = new[]
                    {
                        new KeyValuePair<string, string>("Mongo:Url", mongoUrl),
                        new KeyValuePair<string, string>("Mongo:DatabaseName", databaseName)
                    };
                }).Build();


            dbContext = new MongoDbContext(mongoClient, configuration, logger);
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new Core.Mappings.MappingProfile());
                mc.AddProfile(new API.Mappings.MappingProfile());
                mc.AddProfile(new api.Persistence.Mappings.MappingProfile());
            });

            mapper = Substitute.ForPartsOf<Mapper>(mapperConfig);
            
            dbContext.GridFsBucket.Drop();
            dbContext.MongoClient.DropDatabase(databaseName);

            cancellationTokenSource = new CancellationTokenSource();
            streamCompare = new StreamCompare();
        }

        public abstract BaseRepository<T, TDAL> GetRepository(IMongoDbContext dbContext, IMapper mapper);

        [Fact]
        public virtual async Task CreateAsync_WithNullParameter_ShouldThrow()
        {
            var baseRepository = GetRepository(dbContext, Substitute.For<IMapper>());

            await Assert.ThrowsAsync<ArgumentNullException>(() => baseRepository.CreateAsync(null));
        }

        [Fact]
        public virtual async Task CreateAsync_WithNotNullParameter_ShouldInsertAsyncCalled()
        {
            var expectedEntity = new T();

            var baseRepository = GetRepository(dbContext, mapper);

            var entity = await baseRepository.CreateAsync(expectedEntity);
            var entities = (await (await dbContext.GetCollection<TDAL>(typeof(TDAL).Name)
                .FindAsync(FilterDefinition<TDAL>.Empty))
                .ToListAsync())
                .Select(mapper.Map<TDAL, T>)
                .ToList();
            entities.Count.Should().Be(1);
            Assert.True(entity.Equals(entities.First()));
        }

        [Fact]
        public virtual async Task CreateAsync_WithMongoException_ShouldRollBackAndThrow()
        {
            var session = Substitute.For<IClientSessionHandle>();
            var dbContextMock = Substitute.For<IMongoDbContext>();
            var mongoClientMock = Substitute.For<IMongoClient>();
            var mongoCollectionMock = Substitute.For<IMongoCollection<TDAL>>();
            dbContextMock.GetCollection<TDAL>(typeof(TDAL).Name).Returns(mongoCollectionMock);
            mongoClientMock.StartSession().Returns(session);

            dbContextMock.MongoClient.Returns(mongoClientMock);
            dbContextMock.GetCollection<TDAL>(typeof(TDAL).Name).Returns(mongoCollectionMock);
            mongoCollectionMock.InsertOneAsync(session, default).ThrowsForAnyArgs(new MongoException("Mongo error"));

            var entity = new T();

            var baseRepository = GetRepository(dbContextMock, mapper);
            baseRepository.When(b => b.RollBackAsync()).DoNotCallBase();

            await Assert.ThrowsAsync<MongoException>(() => baseRepository.CreateAsync(entity));
            await baseRepository.Received(1).RollBackAsync();
        }

        [Fact]
        public virtual async Task CreateAsync_WithNotNullParameter_ShouldReturnNewId()
        {
            var expectedEntity = new T();

            var baseRepository = GetRepository(dbContext, mapper);

            var entity = await baseRepository.CreateAsync(expectedEntity);

            var entities = (await (await dbContext.GetCollection<TDAL>(typeof(TDAL).Name)
                        .FindAsync(FilterDefinition<TDAL>.Empty))
                    .ToListAsync())
                .Select(mapper.Map<TDAL, T>)
                .ToList();
            Assert.True(entity.Equals(entities.First()));
        }

        [Fact]
        public virtual async Task DeleteAsync_WithNullParameter_ShouldThrow()
        {
            var baseRepository = GetRepository(dbContext, Substitute.For<IMapper>());

            await Assert.ThrowsAsync<ArgumentNullException>(() => baseRepository.DeleteAsync(null));
        }

        [Fact]
        public virtual async Task DeleteAsync_WithNotNullParameter_ShouldDeleteAsyncCalled()
        {
            var entityDal = new TDAL();
            await dbContext.GetCollection<TDAL>(typeof(T).Name).InsertOneAsync(entityDal);
            var filter = Builders<TDAL>.Filter.Eq("id", entityDal.Id);
            var baseRepository = GetRepository(dbContext, mapper);

            await baseRepository.DeleteAsync(entityDal.Id.ToString());
            var entities =
                await (await dbContext.GetCollection<TDAL>(typeof(TDAL).Name).FindAsync(filter)).ToListAsync();
            entities.Count.Should().Be(0);
        }

        [Fact]
        public virtual async Task DeleteAsync_WithMongoException_ShouldRollBackAndThrow()
        {
            var session = Substitute.For<IClientSessionHandle>();
            var dbContextMock = Substitute.For<IMongoDbContext>();
            var mongoClientMock = Substitute.For<IMongoClient>();
            var mongoCollectionMock = Substitute.For<IMongoCollection<TDAL>>();
            dbContextMock.GetCollection<TDAL>(typeof(TDAL).Name).Returns(mongoCollectionMock);
            mongoClientMock.StartSession().Returns(session);

            dbContextMock.MongoClient.Returns(mongoClientMock);
            mongoCollectionMock.DeleteOneAsync(session, default).ThrowsForAnyArgs(new MongoException("Mongo error"));

            var entity = new T();
            
            var baseRepository = GetRepository(dbContextMock, mapper);
            baseRepository.When(b => b.RollBackAsync()).DoNotCallBase();

            await Assert.ThrowsAsync<MongoException>(() => baseRepository.DeleteAsync(ObjectId.Empty.ToString()));
            await baseRepository.Received(1).RollBackAsync();
        }

        [Fact]
        public virtual async Task GetAllAsync_ShouldCallFindAsync()
        {
            var expectedEntities = new[]
            {
                new T(),
                new T(),
                new T()
            }.Select(mapper.Map<TDAL>).ToList();
            var baseRepository = GetRepository(dbContext, mapper);
            await dbContext.GetCollection<TDAL>(typeof(TDAL).Name).InsertManyAsync(expectedEntities);

            var result = await baseRepository.GetAllAsync(cancellationTokenSource.Token);

            var entities = await (await dbContext.GetCollection<TDAL>(typeof(TDAL).Name).FindAsync(FilterDefinition<TDAL>.Empty)).ToListAsync();

            entities
                .Select(mapper.Map<TDAL, T>)
                .ToList().Should().BeEquivalentTo(result.ToList());
        }

        [Fact]
        public virtual async Task GetByIdAsync_WithNullParameter_ShouldThrow()
        {
            var baseRepository = GetRepository(dbContext, Substitute.For<IMapper>());

            await Assert.ThrowsAsync<ArgumentNullException>(() => baseRepository.GetByIdAsync(null));
        }

        [Fact]
        public virtual async Task GetByIdAsync_WithNotNullParameter_ShouldFindAsyncCalled()
        {
            var expectedEntities = (new T[]
            {
                new T(),
                new T(),
                new T(),
            }).Select(mapper.Map<TDAL>).ToList();
            await dbContext.GetCollection<TDAL>(typeof(TDAL).Name).InsertManyAsync(expectedEntities);
            var baseRepository = GetRepository(dbContext, mapper);

            var result = await baseRepository.GetByIdAsync(expectedEntities[0].Id.ToString());
            result.Should().NotBeNull();
            Assert.True(result.Equals(mapper.Map<T>(expectedEntities.First())));
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbContext.MongoClient.DropDatabase(databaseName);
            }
        }

        public virtual void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}