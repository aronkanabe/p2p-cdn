﻿using System;
using AutoMapper;
using p2p_cdn.api.API.DTO;
using p2p_cdn.api.Core.Models;

namespace p2p_cdn.api.API.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<DataCreationDTO, Data>()
                .ForMember(data => data.Guid, map => map.MapFrom(dataCreationDTO => Guid.NewGuid()))
                .ForMember(data => data.Content, map => map.MapFrom(dataCreationDTO => Convert.FromBase64String(dataCreationDTO.Base64Content)));

            CreateMap<Data, DataOutgoinDTO>();
            CreateMap<TorrentFile, TorrentFileDto>()
                .ForSourceMember(torrentFile => torrentFile.Id, map => map.DoNotValidate());

            CreateMap<TorrentFile, TorrentFileDetailedDto>()
                .ForMember(torrentFile => torrentFile.Content,
                    map => map.MapFrom(torrentFileDetailedDto => torrentFileDetailedDto.Content));
        }
    }
}
