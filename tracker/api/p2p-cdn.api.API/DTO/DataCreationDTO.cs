﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace p2p_cdn.api.API.DTO
{
    public class DataCreationDTO
    {
        [Required]
        [StringLength(maximumLength: 255,MinimumLength = 1)]
        public string OriginalName { get; set; }

        [Required]
        public string Base64Content { get; set; }
    }
}
