﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace p2p_cdn.api.API.DTO
{
    public class TorrentFileDetailedDto : TorrentFileDto
    {
        public byte[] Content { get; set; }
    }
}
