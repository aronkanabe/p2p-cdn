﻿using System;

namespace p2p_cdn.api.API.DTO
{
    public class DataOutgoinDTO
    { 
        public string OriginalName { get; set; }

        public Guid Guid { get; set; }
    }
}
