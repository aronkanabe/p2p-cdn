﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using p2p_cdn.api.API.DTO;
using p2p_cdn.api.Core.UseCases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using p2p_cdn.api.Core.Contracts;
using p2p_cdn.api.Core.Exceptions;

namespace p2p_cdn.api.API.Controllers
{
    [ApiController]
    [Route("/api/[controller]s")]
    public class TorrentFileController : ControllerBase
    {
        private ITorrentFileService torrentFileService;
        private readonly IMapper mapper;

        public TorrentFileController(ITorrentFileService torrentFileService, IMapper mapper)
        {
            this.torrentFileService = torrentFileService;
            this.mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<TorrentFileDto>>> FindAllTorrentFiles(CancellationToken cancellationToken = default)
        {
            IEnumerable<TorrentFileDto> torrentFiles;
            
            try
            {
                torrentFiles = (await torrentFileService.FindAllAsync(cancellationToken))
                    .Select(mapper.Map<TorrentFileDto>);
            }
            catch (TaskCanceledException e)
            {
                return StatusCode(406);
            }
            catch (ParameterException e)
            {
                return BadRequest();
            }
            catch (Exception e)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }

            return Ok(torrentFiles);
        }
        
        [Route("{id:guid}")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<TorrentFileDto>>> FindTorrentFileById([FromRoute]Guid id, CancellationToken cancellationToken = default)
        {
            TorrentFileDetailedDto torrentFileDetailedDto;
            try
            {
                var torrentFile = await torrentFileService.FindByIdAsync(id, cancellationToken);
                torrentFileDetailedDto = mapper.Map<TorrentFileDetailedDto>(torrentFile);
            }
            catch (TaskCanceledException e)
            {
                return StatusCode(406);
            }
            catch (ParameterException e)
            {
                return BadRequest();
            }
            catch (Exception e)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }

            return Ok(torrentFileDetailedDto);
        }
    }
}
