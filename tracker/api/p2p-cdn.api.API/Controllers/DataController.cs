﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using p2p_cdn.api.API.DTO;
using p2p_cdn.api.Core.Models;
using p2p_cdn.api.Core.UseCases;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using p2p_cdn.api.Core.Contracts;
using p2p_cdn.api.Core.Exceptions;

namespace p2p_cdn.api.API.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class DataController : ControllerBase
    {
        private readonly IDataService dataService;
        private readonly IMapper mapper;

        public DataController(IDataService dataByteService, IMapper mapper)
        {
            this.dataService = dataByteService;
            this.mapper = mapper;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<DataOutgoinDTO>> UploadData([FromBody] DataCreationDTO dataCreationDto, CancellationToken cancellationToken)
        {
            if (dataCreationDto == null) return BadRequest("Argument cannot be null!");
            if (!ModelState.IsValid) return BadRequest(ModelState);

            DataOutgoinDTO dataOutgoinDto;
            
            try
            {
                dataOutgoinDto = mapper.Map<DataOutgoinDTO>(
                    await dataService.InsertDataAsync(mapper.Map<Data>(dataCreationDto), cancellationToken));
                if (dataOutgoinDto == null) return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
            catch (TaskCanceledException e)
            {
                return StatusCode(406);
            }
            catch (ParameterException e)
            {
                return BadRequest();
            }
            catch (Exception e)
            {
                return StatusCode(500);
            }

            return CreatedAtAction(nameof(UploadData), dataOutgoinDto);
        }
        
        //create api endpoint for getting data byte range using DataService
        [HttpGet("{guid:guid}")]
        [ProducesResponseType(StatusCodes.Status206PartialContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> GetData([FromRoute]Guid guid, [FromHeader(Name = "Range")]string range, CancellationToken cancellationToken)
        {
            Stream stream;
            try
            {
                stream = await dataService.GetBytesAsync(guid, cancellationToken);
            }
            catch (TaskCanceledException e)
            {
                return StatusCode(406);
            }
            catch (ParameterException e)
            {
                return BadRequest();
            }
            catch (FormatException e)
            {
                return BadRequest();
            }
            catch (Exception e)
            {
                return StatusCode(500);
            }
            
            return File(stream, "application/octet-stream", true);
        }
    }
}
