using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace p2p_cdn.api.API
{
    public class APIMain
    {
        public string[] args;
        public APIMain(string[] args)
        {
            this.args = args;
        }

        public void Run(OnExternalServiceInject onExternalServiceInject)
        {
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Verbose)
                .CreateLogger();

            try
            {
                Log.Information("Starting web host");
                CreateHostBuilder(args, onExternalServiceInject).Build().Run();
            } catch (Exception e)
            {
                Log.Fatal(e, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args, OnExternalServiceInject onExternalServiceInject) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices(onExternalServiceInject.Invoke)
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
