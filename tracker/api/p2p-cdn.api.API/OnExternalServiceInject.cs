﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace p2p_cdn.api.API
{
    public delegate void OnExternalServiceInject(HostBuilderContext context, IServiceCollection services);
}
