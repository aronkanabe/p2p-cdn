﻿using NSubstitute;
using NSubstitute.ExceptionExtensions;
using p2p_cdn.api.Core.Contracts;
using p2p_cdn.api.Core.Models;
using p2p_cdn.api.Core.UseCases;
using p2p_cdn.api.Persistence;
using p2p_cdn.api.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using MongoDB.Bson;
using NSubstitute.ReceivedExtensions;
using Xunit;

namespace p2p_cdn.api.UnitTest.Core
{
    public class DataServiceTests
    {
        protected readonly CancellationTokenSource cancellationTokenSource;

        public DataServiceTests()
        {
            cancellationTokenSource = new CancellationTokenSource();
        }
        public static IEnumerable<object[]> TestDataFactory(IList<Guid> guids, IList<string> originalNames, IList<Stream> contents)
        {
            if (guids.Count != originalNames.Count || guids.Count != contents.Count)
            {
                throw new ArgumentOutOfRangeException();
            }

            for (var i = 0; i < guids.Count; ++i)
            {
                var data = new Data
                {
                    Guid = guids[i],
                    OriginalName = originalNames[i],
                    Content = contents[i]
                };

                yield return new object[]
                {
                    data
                };
            }

        }

        public static IEnumerable<object[]> CorrectEmptyData()
        {
            var guids = new[]
            {
                Guid.NewGuid()
            };

            var originalNames = new[]
            {
                "Test memory"
            };

            var contents = new Stream[]
            {
                new MemoryStream()
            };

            return TestDataFactory(guids, originalNames, contents);
        }
        
        public static IEnumerable<object[]> InCorrectEmptyData()
        {
            var guids = new Guid[]
            {
                Guid.Empty
            };

            var originalNames = new string[]
            {
                null
            };

            var contents = new Stream[]
            {
                Stream.Null
            };

            return TestDataFactory(guids, originalNames, contents);
        }

        private static IUnitOfWork GetUnitOfWorkMock()
        {
            var unitOfWorkMock = Substitute.For<IUnitOfWork>();
            unitOfWorkMock.DataRepository.Returns(Substitute.For<IDataRepository>());
            unitOfWorkMock.TorrentFileRepository.Returns(Substitute.For<ITorrentFileRepository>());
            return unitOfWorkMock;
        }

        [Fact]
        public void Constructor_WithNonNullValues_ShouldNotThrow()
        {
            var exception = Record.Exception(() => new DataService(Substitute.For<IUnitOfWork>(), Substitute.For<ITorrentFileService>()));
            
            Assert.Null(exception);
        }

        [Theory]
        [MemberData(nameof(CorrectEmptyData))]
        public async Task InsertData_CorrectEmptyData_NotThrow(Data data)
        {
            var unitOfWork = GetUnitOfWorkMock();
            var torrentFileRepository = unitOfWork.TorrentFileRepository;
            torrentFileRepository.CreateAsync(default, cancellationTokenSource.Token).ReturnsForAnyArgs(new TorrentFile());
            var torrentFileService = Substitute.For<ITorrentFileService>();
            torrentFileService.CreateAsync(default, default).ReturnsForAnyArgs(default(TorrentFile));
            // objectId value of 'x'
            unitOfWork.DataRepository.CreateAsync(default).ReturnsForAnyArgs(new Data()
            );
            var dataService = new DataService(unitOfWork, torrentFileService);
            
            
            var exception = await Record.ExceptionAsync( async () => await dataService.InsertDataAsync(data, cancellationTokenSource.Token));
            
            Assert.Null(exception);
            await unitOfWork.DidNotReceive().RollRollBackAsync();
            unitOfWork.Received(1).StartTransaction();
            await unitOfWork.ReceivedWithAnyArgs(1).SaveAsync();
        }

        [Theory]
        [MemberData(nameof(InCorrectEmptyData))]
        public async Task InsertData_IncorrectEmptyData_DataRepository_Throws_ArgumentNull(Data data)
        {
            var unitOfWork = GetUnitOfWorkMock();
            var torrentFileRepository = unitOfWork.TorrentFileRepository;
            var torrentFileService = Substitute.For<ITorrentFileService>();
            var dataService = new DataService(unitOfWork, torrentFileService);
            unitOfWork.DataRepository.CreateAsync(default).ThrowsForAnyArgs<ArgumentNullException>();

            await Assert.ThrowsAsync<ArgumentNullException>(async () => await dataService.InsertDataAsync(data, cancellationTokenSource.Token));
            await unitOfWork.ReceivedWithAnyArgs(1).RollRollBackAsync();
        }
        
        [Fact]
        public async Task InsertData_WithNullData_Throws_ArgumentNull()
        {
            var unitOfWork = GetUnitOfWorkMock();
            var torrentFileRepository = unitOfWork.TorrentFileRepository;
            torrentFileRepository.CreateAsync(default, cancellationTokenSource.Token).ThrowsForAnyArgs<ArgumentNullException>();
            var torrentFileService = Substitute.For<ITorrentFileService>();
            torrentFileService.CreateAsync(default, default).ReturnsForAnyArgs(default(TorrentFile));
            var dataService = new DataService(unitOfWork, torrentFileService);
            unitOfWork.DataRepository.CreateAsync(default).ReturnsForAnyArgs(new Data());

            await Assert.ThrowsAsync<ArgumentNullException>(async () => await dataService.InsertDataAsync(null, cancellationTokenSource.Token));
        }
    }
}
