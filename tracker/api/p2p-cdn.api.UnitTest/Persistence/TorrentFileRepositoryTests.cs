﻿using AutoMapper;
using p2p_cdn.api.Core.Models;
using p2p_cdn.api.Persistence;
using p2p_cdn.api.Persistence.DAL;
using p2p_cdn.api.Persistence.Repositories;

namespace p2p_cdn.api.UnitTest.Persistence
{
    public class TorrentFileRepositoryTests: BaseRepositoryTests<TorrentFile,TorrentFileDAL>
    {
        public override BaseRepository<TorrentFile, TorrentFileDAL> GetRepository(IMongoDbContext dbContext, IMapper mapper)
        {
            return new TorrentFileRepository(dbContext, mapper);
        }
    }
}