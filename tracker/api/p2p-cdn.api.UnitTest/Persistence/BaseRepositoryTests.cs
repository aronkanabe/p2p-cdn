﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentAssertions;
using MongoDB.Bson;
using MongoDB.Driver;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NSubstitute.ReceivedExtensions;
using p2p_cdn.api.Core.Models;
using p2p_cdn.api.Persistence;
using p2p_cdn.api.Persistence.DAL;
using p2p_cdn.api.Persistence.Repositories;
using Xunit;

namespace p2p_cdn.api.UnitTest.Persistence
{
    public abstract class BaseRepositoryTests<T, TDAL> where T: BaseModel, new() where TDAL: BaseDAL, new()
    {
        private IMongoDbContext dbContext;
        private readonly IMongoClient mongoClient;
        private IMapper mapper;

        public BaseRepositoryTests()
        {
            dbContext = Substitute.For<IMongoDbContext>();
            mongoClient = Substitute.For<IMongoClient>();
            dbContext.MongoClient.Returns(mongoClient);
            
            mapper = Substitute.For<IMapper>();
        }
        
        public abstract BaseRepository<T, TDAL> GetRepository(IMongoDbContext dbContext, IMapper mapper);
        
        [Fact]
        public void Constructor_WithNonNullValues_ShouldNotThrow()
        {
            var exception = Record.Exception(() =>
                GetRepository(Substitute.For<IMongoDbContext>(), mapper));

            exception.Should().BeNull();
        }

        [Fact]
        public void Constructor_WithNullDbContext_ShouldThrow()
        {
            Action construct = () => GetRepository(null, mapper);

            construct.Should().Throw<ArgumentNullException>();
        }

        [Fact]
        public void Constructor_WithNullMapper_ShouldThrow()
        {
            Action construct = () => GetRepository(Substitute.For<IMongoDbContext>(), null);

            construct.Should().Throw<ArgumentNullException>();
        }

        [Fact]
        public void SessionGet_ShouldReturnSameSession()
        {
            var baseRepository = GetRepository(dbContext, mapper);

            var session = baseRepository.Session;

            var session2 = baseRepository.Session;

            session.Should().BeEquivalentTo(session2);
        }

        [Fact]
        public void SessionSet_WithNullSession_ShouldSetValue()
        {
            var baseRepository = GetRepository(dbContext, mapper);

            var expectedSession = Substitute.For<IClientSessionHandle>();

            baseRepository.Session = null;

            baseRepository.Session = expectedSession;

            baseRepository.Session.Should().Be(expectedSession);
        }

        [Fact]
        public void SessionSet_WithNotNullSession_ShouldSetValue()
        {
            var baseRepository = GetRepository(dbContext, mapper);
            var oldSession = Substitute.For<IClientSessionHandle>();
            baseRepository.Session = oldSession;
            
            
            var expectedSession = Substitute.For<IClientSessionHandle>();
            baseRepository.Session = expectedSession;

            baseRepository.Session.Should().Be(expectedSession);
            oldSession.Received(1).Dispose();
        }

        [Fact]
        public void SessionSet_WithSessionInTransaction_ShouldAbortTransaction()
        {
            var baseRepository = GetRepository(dbContext, mapper);
            var oldSession = Substitute.For<IClientSessionHandle>();
            oldSession.IsInTransaction.Returns(true);
            baseRepository.Session = oldSession;
            
            
            var expectedSession = Substitute.For<IClientSessionHandle>();
            baseRepository.Session = expectedSession;

            baseRepository.Session.Should().Be(expectedSession);
            oldSession.Received(1).Dispose();
            oldSession.Received(1).AbortTransaction();
        }

        [Fact]
        public void SessionSet_WithNullSession_ShouldNotThrow()
        {
            var baseRepository = GetRepository(dbContext, mapper);
            baseRepository.Session = null;
            var expectedSession = Substitute.For<IClientSessionHandle>();
            
            var exception = Record.Exception(() => baseRepository.Session = expectedSession);

            Assert.Null(exception);
        }

        [Fact]
        public void SessionSet_WithNotNullSession_ShouldNotThrow()
        {
            var baseRepository = GetRepository(dbContext, mapper);
            var oldSession = Substitute.For<IClientSessionHandle>();
            baseRepository.Session = oldSession;
            var expectedSession = Substitute.For<IClientSessionHandle>();
            
            var exception = Record.Exception(() => baseRepository.Session = expectedSession);

            Assert.Null(exception);
        }

        [Fact]
        public async Task RollBackAsync_WithoutParameter_ShouldCallAbortTransactionAsync()
        {
            var session = Substitute.For<IClientSessionHandle>();
            mongoClient.StartSession().Returns(session);
            var baseRepository = GetRepository(dbContext, mapper);

            await baseRepository.RollBackAsync();

            await session.Received(1).AbortTransactionAsync();
        }
        
        [Fact]
        public async Task RollBackAsync_WithCancellationToken_ShouldCallAbortTransactionAsync()
        {
            var session = Substitute.For<IClientSessionHandle>();
            mongoClient.StartSession().Returns(session);
            var baseRepository = GetRepository(dbContext, mapper);
            var cancellationTokenSource = new CancellationTokenSource();
            
            await baseRepository.RollBackAsync(cancellationTokenSource.Token);

            await session.Received(1).AbortTransactionAsync(cancellationTokenSource.Token);
        }

        [Fact]
        public async Task RollBackAsync_WithCancellationTokenCanceled_ShouldThrowTaskCanceledException()
        {
            var session = Substitute.For<IClientSessionHandle>();
            mongoClient.StartSession().Returns(session);
            var baseRepository = GetRepository(dbContext, mapper);
            var cancellationTokenSource = new CancellationTokenSource();
            
            cancellationTokenSource.Cancel();
            var exception = Record.ExceptionAsync(async () => await baseRepository.RollBackAsync(cancellationTokenSource.Token));

            await session.Received(1).AbortTransactionAsync(cancellationTokenSource.Token);
            exception.Exception?.InnerException.Should().BeOfType<TaskCanceledException>();
        }
        
    }
}
