﻿using AutoMapper;
using p2p_cdn.api.Core.Models;
using p2p_cdn.api.Persistence;
using p2p_cdn.api.Persistence.DAL;
using p2p_cdn.api.Persistence.Repositories;

namespace p2p_cdn.api.UnitTest.Persistence
{
    public class DataRepositoryTests: BaseRepositoryTests<Data,DataDAL>
    {
        public override BaseRepository<Data, DataDAL> GetRepository(IMongoDbContext dbContext, IMapper mapper)
        {
            return new DataRepository(dbContext, mapper);
        }
    }
}