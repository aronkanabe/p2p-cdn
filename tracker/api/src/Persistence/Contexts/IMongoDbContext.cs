using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace p2p_tracker.Persistence.Contexts
{
    public interface IMongoDbContext
    {
         IMongoCollection<T> GetCollection<T>(string name);

         IClientSessionHandle Session { get; }

        IGridFSBucket GridFSBucket { get; }
    }
}