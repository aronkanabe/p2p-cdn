using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace p2p_tracker.Persistence.Contexts
{
    public class MongoDbContext : IMongoDbContext
    {
        protected IMongoDatabase database { get; set; }

        protected MongoClient mongoClient { get; set; }

        protected GridFSBucket gridFSBucket;

        public IGridFSBucket GridFSBucket => gridFSBucket;

        public IClientSessionHandle Session => mongoClient.StartSession();

        public MongoDbContext(MongoClient mongoClient, IOptions<MongoDatabaseName> databaseName)
        {
            this.mongoClient = mongoClient;
            database = mongoClient.GetDatabase(databaseName.Value.Name);
            gridFSBucket = new GridFSBucket(database, new GridFSBucketOptions
            {
                //TODO: extract to configuration
                BucketName = "MongoStorage",
                ChunkSizeBytes = 262144, // 256kb
                WriteConcern = WriteConcern.WMajority,
                ReadPreference = ReadPreference.SecondaryPreferred
            });
        }

        public IMongoCollection<T> GetCollection<T>(string name)
        {
            return database.GetCollection<T>(name);
        }
    }
}