using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using MongoDB.Driver;
using p2p_tracker.Persistence.Contexts;
using p2p_tracker.Repositories;
using p2p_tracker.Repositories.Interfaces;
using p2p_tracker.Services;
using p2p_tracker.Services.Interfaces;

namespace p2p_tracker
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "p2p_tracker", Version = "v1" });
            });

            Console.WriteLine((Configuration.GetSection("MONGO_URL").Value));

            services.AddSingleton<MongoClient>(new MongoClient(Configuration.GetSection("MONGO_URL").Value));

            services.Configure<MongoDatabaseName>(mongoDatabaseName =>
            {
                mongoDatabaseName.Name = Configuration.GetSection("MONGO_DATABASE_NAME").Value;
            });

            services.AddScoped<IMongoDbContext, MongoDbContext>();

            services.AddScoped<IDataRepository, DataMongoRepository>();
            services.AddScoped<IDataService, DataService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "p2p_tracker v1"));
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
