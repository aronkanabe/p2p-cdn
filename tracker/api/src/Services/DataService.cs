using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using p2p_tracker.Models;
using p2p_tracker.Repositories.Interfaces;
using p2p_tracker.Services.Interfaces;

namespace p2p_tracker.Services
{
    public class DataService : IDataService
    {
        private readonly IDataRepository dataRepository;

        public DataService(IDataRepository dataRepository)
        {
            this.dataRepository = dataRepository;
        }

        public async Task<IEnumerable<Data>> FindAsync()
        {
            return await dataRepository.FindAllAsync();
        }

        public async Task<Data> FindAsync(Guid guid)
        {
            return await dataRepository.FindByAsync(guid);
        }

        public async Task<Data> InsertAsync(Stream stream, string originalName = null, IList<string> tags = null)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("Stream object is null");
            }
            return await dataRepository.Insert(stream, originalName, tags);
        }

    }
}