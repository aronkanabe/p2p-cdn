using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using p2p_tracker.Models;

namespace p2p_tracker.Services.Interfaces
{
    public interface IDataService
    {
        Task<IEnumerable<Data>> FindAsync();
        
        Task<Data> FindAsync(Guid guid);

        Task<Data> InsertAsync(Stream stream, string originalName = null, IList<string> tags = null);
    }
}