using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using p2p_tracker.Models;

namespace p2p_tracker.Repositories.Interfaces
{
    public interface IDataRepository: IBaseRepository<Data>
    {
        Task<Data> FindByAsync(Guid guid);
        Stream GetDataStream(Guid guid);
        Task<Data> Insert(Stream stream, string originalName, IList<string> tags);
    }
}