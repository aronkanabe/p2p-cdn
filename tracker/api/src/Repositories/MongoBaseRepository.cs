using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using p2p_tracker.Persistence.Contexts;
using p2p_tracker.Repositories.Interfaces;

namespace p2p_tracker.Repositories
{
    public abstract class MongoBaseRepository<T> : IBaseRepository<T> where T: class
    {
        protected readonly IMongoDbContext dbContext;

        protected IMongoCollection<T> dbCollection;

        protected MongoBaseRepository(IMongoDbContext context)
        {
            dbContext = context;
            dbCollection = dbContext.GetCollection<T>(typeof(T).Name);
        }

        public virtual void Delete(T entity)
        {
            dbCollection.DeleteOne(Builders<T>.Filter.Where(databaseEntity => CompareById(databaseEntity, entity)));
        }

        public virtual async Task<IEnumerable<T>> FindAllAsync()
        {
            return await dbCollection.AsQueryable().ToListAsync();
        }

        public virtual async Task<IEnumerable<T>> FindByAsync(Expression<Func<T, bool>> predicate)
        {
            FilterDefinition<T> filter = Builders<T>.Filter.Where(predicate);
            return await (await dbCollection.FindAsync<T>(dbContext.Session, filter)).ToListAsync();
        }

        public virtual T Insert(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(typeof(T).Name + "object is null");
            }
            dbCollection.InsertOne(dbContext.Session, entity);

            return entity;
        }

        public virtual T Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(typeof(T).Name + "object is null");
            }
            
            ReplaceOneResult replaceOneResult = dbCollection.ReplaceOne(dbContext.Session, Builders<T>.Filter.Where(databaseEntity => CompareById(databaseEntity, entity)), entity);
            if (!replaceOneResult.IsAcknowledged || replaceOneResult.ModifiedCount == 1)
            {
                throw new MongoException($"Update of {typeof(T).Name}  is failed");
            }

            return entity;
        }

        public abstract bool CompareById(T firstEntity, T secondEntity);
    }
}