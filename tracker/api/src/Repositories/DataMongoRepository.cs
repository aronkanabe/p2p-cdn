using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using p2p_tracker.Models;
using p2p_tracker.Persistence.Contexts;
using p2p_tracker.Repositories.Interfaces;

namespace p2p_tracker.Repositories
{
    public class DataMongoRepository : MongoBaseRepository<Data>, IDataRepository
    {
        public DataMongoRepository(IMongoDbContext context) : base(context)
        {
        }

        public override bool CompareById(Data firstEntity, Data secondEntity)
        {
            return firstEntity.Guid == secondEntity.Guid;
        }

        public async Task<Data> FindByAsync(Guid guid)
        {
            return (await FindByAsync(databaseEntity => databaseEntity.Guid == guid)).Single();
        }

        public Stream GetDataStream(Guid guid)
        {
            return dbContext.GridFSBucket.OpenDownloadStreamByName(guid.ToString());
        }

        public async Task<Data> Insert(Stream stream, string originalName, IList<string> tags)
        {
            Data data = new Data();
            data.Guid = Guid.NewGuid();
            data.OriginalName = originalName;
            data.tags = tags;

            using (var session = dbContext.Session)
            {
                session.StartTransaction();
                
                var gridFSUpload = dbContext.GridFSBucket.UploadFromStreamAsync(data.Guid.ToString(), stream);

                Insert(data);

                await gridFSUpload;
                session.CommitTransaction();
            }

            return data;
        }

        public Data Update(Stream stream, Data data)
        {
            using (var session = dbContext.Session)
            {
                session.StartTransaction();

                dbContext.GridFSBucket.UploadFromStream(data.Guid.ToString(), stream);

                Update(data);

                session.CommitTransaction();
            }

            return data;
        }

        public override void Delete(Data data)
        {
            using (var session = dbContext.Session)
            {
                session.StartTransaction();
                
                var fileInfo = dbContext.GridFSBucket.Find(Builders<GridFSFileInfo>.Filter.Eq(file => file.Filename, data.Guid.ToString()));

                fileInfo.ToList().ForEach(fileInfo => dbContext.GridFSBucket.Delete(fileInfo.Id));

                base.Delete(data);

                session.CommitTransaction();
            }
        }
    }
}