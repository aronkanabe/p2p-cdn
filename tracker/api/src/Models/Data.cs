using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace p2p_tracker.Models
{
    public class Data
    {
        public string OriginalName { get; set; }

        [BsonId]
        public Guid Guid { get; set; }

        public IList<string> tags { get; set; }
    }
}