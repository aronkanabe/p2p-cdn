using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using p2p_tracker.Models;
using p2p_tracker.Services.Interfaces;

namespace p2p_tracker.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DataController : ControllerBase
    {
        private readonly ILogger<DataController> logger;

        private readonly IDataService dataService;

        public DataController(ILogger<DataController> logger, IDataService dataService)
        {
            this.logger = logger;
            this.dataService = dataService;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IEnumerable<Data>> GetAllAsync()
        {
            return await dataService.FindAsync();
        }

        [HttpGet("/{guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<Data> GetByIdAsync(Guid guid)
        {
            return await dataService.FindAsync(guid);
        }

        [HttpPost, DisableRequestSizeLimit]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Data>> InsertData(IFormFile contentFile)
        {
            if (contentFile == null)
            {
                return BadRequest();
            }
            
            if (contentFile.Length > 0)
            {
                return await dataService.InsertAsync(contentFile.OpenReadStream(), contentFile.FileName);
            }
            
            return BadRequest();
        }
    }
}