﻿using System.Net.Http;
using Xunit;

namespace p2p_cdn.api.APITests
{
    public abstract class IntegrationTest : IClassFixture<ApiWebApplicationFactory>
    {
        protected readonly ApiWebApplicationFactory factory;

        protected readonly HttpClient client;

        protected IntegrationTest(ApiWebApplicationFactory factory)
        {
            this.factory = factory;
            client = factory.CreateClient();
        }
    }
}
