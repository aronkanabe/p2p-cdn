﻿using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace p2p_cdn.api.APITests
{
    public static class Extensions
    {
        public static async Task<T> GetAndDeserialize<T>(this HttpClient client, string requestUri)
        {
            return await client.GetFromJsonAsync<T>(requestUri);
        }
    }
}
