﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using p2p_cdn.api.API;

[assembly: WebApplicationFactoryContentRoot("p2p-tracker, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "./", "p2p_cdn.tracker.IntegrationTest.dll", "1")]
namespace p2p_cdn.api.APITests
{
    public class ApiWebApplicationFactory : WebApplicationFactory<Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureTestServices(servoces =>
            {
            });
        }
    }
}