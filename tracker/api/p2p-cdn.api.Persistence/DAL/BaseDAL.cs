﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace p2p_cdn.api.Persistence.DAL
{
    public class BaseDAL
    {
        [BsonId]
        public ObjectId Id { get; set; }
    }
}