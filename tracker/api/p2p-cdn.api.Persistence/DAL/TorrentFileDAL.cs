﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace p2p_cdn.api.Persistence.DAL
{
    [BsonIgnoreExtraElements]
    public class TorrentFileDAL : BaseDAL
    {
        public string Guid { get; set; }
        
        public string OriginalName { get; set; }
        
        public byte[] Content { get; set; }

        public IDictionary<string, string> Tags { get; set; }
    }
}
