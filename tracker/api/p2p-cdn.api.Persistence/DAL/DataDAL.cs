﻿using p2p_cdn.api.Persistence.DAL;

namespace p2p_cdn.api.Persistence.DAL
{
    public class DataDAL: BaseDAL
    {
        public string OriginalName { get; set; }

        public string Guid { get; set; }

        public string Base64Content { get; set; }
    }
}