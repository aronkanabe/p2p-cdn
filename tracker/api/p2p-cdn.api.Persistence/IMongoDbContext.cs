﻿using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace p2p_cdn.api.Persistence
{
    public interface IMongoDbContext
    {
        void CreateCollection<T>(string collectionName);
        bool CollectionExists(string collectionName);
        IMongoCollection<T> GetCollection<T>(string collectionName);
        IMongoClient MongoClient { get; set; }
        IGridFSBucket GridFsBucket { get; }
    }
}