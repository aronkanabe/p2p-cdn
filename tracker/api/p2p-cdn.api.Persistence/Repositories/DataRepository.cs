﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using p2p_cdn.api.Core.Contracts;
using p2p_cdn.api.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using p2p_cdn.api.Persistence.DAL;

namespace p2p_cdn.api.Persistence.Repositories
{
    public class DataRepository : BaseRepository<Data, DataDAL>, IDataRepository
    {
        public DataRepository(IMongoDbContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            session = dbContext.MongoClient.StartSession();
        }

        // for mocking
        public DataRepository() : base()
        {
        }

        // TODO: Add id
        public override async Task<Data> CreateAsync(Data data, CancellationToken cancellationToken = default)
        {
            if (data == null) throw new ArgumentNullException(nameof(data));

            var objectId = ObjectId.Empty;
            try
            {
                var options = new GridFSUploadOptions
                {
                    Metadata = new BsonDocument
                    {
                        {"originalName", data.OriginalName}
                    }
                };
                data.Id = (await dbContext.GridFsBucket.UploadFromStreamAsync(data.Guid.ToString(), data.Content,
                    options, cancellationToken)).ToString();
            }
            catch (MongoException e)
            {
                await RollBackAsync(cancellationToken);
                throw;
            }

            return data;
        }

        public override async Task DeleteAsync(string id, CancellationToken cancellationToken = default)
        {
            try
            {
                await dbContext.GridFsBucket.DeleteAsync(ObjectId.Parse(id), cancellationToken);
            }
            catch (MongoException e)
            {
                await RollBackAsync(cancellationToken);
                throw;
            }
        }

        public override async Task<Data> GetByIdAsync(string guid, CancellationToken cancellationToken = default)
        {
            if (guid == null) throw new ArgumentNullException(nameof(guid));
            Data data;
            try
            {
                var filter = Builders<GridFSFileInfo<ObjectId>>.Filter.Eq("filename", guid);
                var gridFsFileInfo =
                    await (await dbContext.GridFsBucket.FindAsync(filter, cancellationToken: cancellationToken))
                        .FirstAsync(cancellationToken: cancellationToken);
                
                var options = new GridFSDownloadOptions
                {
                    Seekable = true
                };
                
                var stream = await dbContext.GridFsBucket.OpenDownloadStreamAsync(gridFsFileInfo.Id, options,
                    cancellationToken);
                data = new Data
                {
                    Id = gridFsFileInfo.Id.ToString(),
                    OriginalName = gridFsFileInfo.Metadata["originalName"].ToString(),
                    Guid = Guid.Parse(gridFsFileInfo.Filename),
                    Content = stream
                };
            }
            catch (MongoException e)
            {
                await RollBackAsync(cancellationToken);
                throw;
            }
            catch (NullReferenceException e)
            {
                await RollBackAsync(cancellationToken);
                throw;
            }
            return data;
        }
        
        public async Task<byte[]> GetByteRangeAsync(Guid guid, int start, int end, CancellationToken cancellationToken = default)
        {
            if (guid == null) throw new ArgumentNullException(nameof(guid));
            var filter = Builders<GridFSFileInfo<ObjectId>>.Filter.Eq("filename", guid.ToString());
            var gridFsFileInfo =
                await (await dbContext.GridFsBucket.FindAsync(filter, cancellationToken: cancellationToken))
                    .FirstAsync(cancellationToken);
            
            var options = new GridFSDownloadOptions
            {
                Seekable = true
            };

            await using var stream = await dbContext.GridFsBucket.OpenDownloadStreamAsync(gridFsFileInfo.Id, options,
                cancellationToken);
            var buffer = new byte[end - start];
            stream.Seek(start, SeekOrigin.Begin);
            await stream.ReadAsync(buffer.AsMemory(0, buffer.Length), cancellationToken);
            await stream.CloseAsync(cancellationToken);
            return buffer;
        }

        public override Task UpdateAsync(Data data, CancellationToken cancellationToken = default)
        {
            throw new System.NotImplementedException();
        }
    }
}