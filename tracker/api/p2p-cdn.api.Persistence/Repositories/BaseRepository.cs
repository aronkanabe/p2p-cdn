﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using p2p_cdn.api.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using p2p_cdn.api.Core.Models;
using p2p_cdn.api.Persistence.DAL;

namespace p2p_cdn.api.Persistence.Repositories
{
    public abstract class BaseRepository<T, TDAL> : IBaseRepository<T> where T: BaseModel where TDAL: BaseDAL
    {

        protected readonly IMongoDbContext dbContext;
        protected readonly IMapper mapper;
        protected readonly IMongoCollection<TDAL> dbCollection;

        protected IClientSessionHandle session;

        public IClientSessionHandle Session { get
            {
                return session;
            }
            set
            {
                if (session != null)
                { 
                    if (session.IsInTransaction)
                    {
                        session?.AbortTransaction();
                    }
                    session?.Dispose();
                }
                session = value;
            }
        }

        public BaseRepository(IMongoDbContext dbContext, IMapper mapper)
        {
            this.dbContext = dbContext ?? throw new ArgumentNullException();
            this.mapper = mapper ?? throw new ArgumentNullException();
            dbContext.CreateCollection<TDAL>(typeof(TDAL).Name);
            dbCollection = dbContext.GetCollection<TDAL>(typeof(TDAL).Name);
            Session = dbContext.MongoClient.StartSession();
        }

        protected BaseRepository()
        {
        }

        public virtual async Task<T> CreateAsync(T entity, CancellationToken cancellationToken = default)
        {
            if (entity == null) throw new ArgumentNullException(typeof(T).Name);

            var baseDal = mapper.Map<TDAL>(entity);
            
            try
            {
                await dbCollection.InsertOneAsync(Session, baseDal, cancellationToken: cancellationToken);
            } catch (MongoException e)
            {
                await RollBackAsync(cancellationToken);
                throw;
            }

            return mapper.Map<TDAL, T>(baseDal);
        }

        public virtual async Task DeleteAsync(string id, CancellationToken cancellationToken = default)
        {
            var filterDefinition = Builders<TDAL>.Filter.Eq("_id", ObjectId.Parse(id));
            await DeleteAsync(filterDefinition, cancellationToken);
        }
        
        protected virtual async Task DeleteAsync(FilterDefinition<TDAL> filter, CancellationToken cancellationToken = default)
        {
            if (filter == null) throw new ArgumentNullException(nameof(filter));
            // check later for assure the deletion

            try
            {
                var deleteResult = await dbCollection.DeleteOneAsync(Session, filter, cancellationToken: cancellationToken);
            } catch(MongoException e)
            {
                await RollBackAsync(cancellationToken);
                throw;
            }
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync(CancellationToken cancellationToken = default)
        {
            return (await dbCollection.FindAsync(FilterDefinition<TDAL>.Empty, cancellationToken: cancellationToken))
                .ToEnumerable(cancellationToken)
                .Select(mapper.Map<T>);
        }

        public virtual async Task<T> GetByIdAsync(string id, CancellationToken cancellationToken = default)
        {
            var filter = Builders<TDAL>.Filter.Eq("_id", ObjectId.Parse(id));
            return await GetByIdAsync(filter, cancellationToken);
        }
        
        protected virtual async Task<T> GetByIdAsync(FilterDefinition<TDAL> filter, CancellationToken cancellationToken = default)
        {
            if (filter == null) throw new ArgumentNullException(nameof(filter));
            
            var asyncCursor = (await dbCollection.FindAsync(filter, cancellationToken: cancellationToken));
            return mapper.Map<T>(await asyncCursor.FirstOrDefaultAsync(cancellationToken));
        }

        public virtual async Task RollBackAsync(CancellationToken cancellationToken = default)
        {
            await Session.AbortTransactionAsync(cancellationToken);
        }

        public virtual async Task SaveAsync(CancellationToken cancellationToken = default)
        {
            await Session.CommitTransactionAsync(cancellationToken);
        }

        public virtual async Task UpdateAsync(T entity, CancellationToken cancellationToken = default)
        {
            if (entity == null) throw new ArgumentNullException(typeof(T).Name);

            var entityDal = mapper.Map<TDAL>(entity);

            var filter = Builders<TDAL>.Filter.Where(document => document.Equals(entityDal));

            try
            {
                var updateResult = await dbCollection.ReplaceOneAsync(Session, filter, entityDal, cancellationToken: cancellationToken);
            } catch (MongoException e)
            {
                await RollBackAsync(cancellationToken);
            }
        }

        public void Dispose()
        {
            Session.Dispose();
        }

        public void StartTransaction()
        {
            if(!session.IsInTransaction)
            {
                session.StartTransaction();
            }
        }
    }
}
