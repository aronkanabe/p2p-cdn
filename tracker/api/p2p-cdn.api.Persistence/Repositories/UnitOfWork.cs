﻿using MongoDB.Driver;
using p2p_cdn.api.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace p2p_cdn.api.Persistence.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IMongoDbContext dbContext;

        private readonly DataRepository dataRepository;

        private readonly TorrentFileRepository torrentFileRepository;

        public IDataRepository DataRepository => dataRepository;

        public ITorrentFileRepository TorrentFileRepository => torrentFileRepository;

        private readonly IClientSessionHandle session;

        public UnitOfWork(IMongoDbContext dbContext, IDataRepository dataByteRepository, ITorrentFileRepository torrentFileRepository)
        {
            this.dbContext = dbContext;
            session = dbContext.MongoClient.StartSession();
            dataRepository = dataByteRepository as DataRepository;
            this.torrentFileRepository = torrentFileRepository as TorrentFileRepository;
               
            // link with common session to able to roll back or commit together
            var repository = this.dataRepository;
            if (repository != null) repository.Session = session;
            
            var fileRepository = this.torrentFileRepository;
            if (fileRepository != null) fileRepository.Session = session;
        }

        public UnitOfWork()
        {
        }

        public void Dispose()
        {
            session.Dispose();
        }

        public async Task RollRollBackAsync(CancellationToken cancellationToken = default)
        {
            await session.AbortTransactionAsync(cancellationToken);
        }

        public async Task SaveAsync(CancellationToken cancellationToken = default)
        {
            await session.CommitTransactionAsync(cancellationToken);
        }

        public void StartTransaction()
        {
            session.StartTransaction();
        }
    }
}
