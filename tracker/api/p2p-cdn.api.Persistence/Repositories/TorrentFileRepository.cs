﻿using AutoMapper;
using p2p_cdn.api.Core.Contracts;
using p2p_cdn.api.Core.Models;
using p2p_cdn.api.Persistence.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using p2p_cdn.api.Core.Exceptions;

namespace p2p_cdn.api.Persistence.Repositories
{
    public class TorrentFileRepository : BaseRepository<TorrentFile, TorrentFileDAL>, ITorrentFileRepository
    {
        public TorrentFileRepository(IMongoDbContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
        }
        // for testing
        public TorrentFileRepository()
        {
            
        }

        public async Task<TorrentFile> GetByIdAsync(Guid guid, CancellationToken cancellationToken = default)
        {
            if (guid == null) throw new ArgumentNullException(nameof(guid));
            var filterDefinition = Builders<TorrentFileDAL>.Filter.Eq("Guid", guid);
            return await GetByIdAsync(filterDefinition, cancellationToken);
        }
    }
}
