﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace p2p_cdn.api.Persistence
{
    public class MongoDbContext: IMongoDbContext
    {
        protected IMongoDatabase Database { get; set; }

        public IMongoClient MongoClient { get; set; }

        private readonly ILogger<MongoDbContext> logger;

        public IGridFSBucket GridFsBucket { get; }

        public MongoDbContext(IMongoClient mongoClient, IConfiguration configuration, ILogger<MongoDbContext> logger)
        {
            MongoClient = mongoClient;
            this.logger = logger;
            logger.LogInformation("MongoDB connection string: {connectionString}", configuration.GetSection("Mongo:Url").Value);
            mongoClient.ListDatabases();
            Database = mongoClient.GetDatabase(configuration.GetSection("Mongo:DatabaseName").Value);
            GridFsBucket = new GridFSBucket(Database, new GridFSBucketOptions
            {
                //TODO: extract to configuration
                BucketName = "MongoStorage",
                ChunkSizeBytes = 262144, // 256kb
                WriteConcern = WriteConcern.WMajority,
                ReadPreference = ReadPreference.SecondaryPreferred
            });
        }

        public void CreateCollection<T>(string collectionName)
        {
            if (!CollectionExists(collectionName))
            {
                Database.CreateCollection(collectionName);
            }
        }

        public bool CollectionExists(string collectionName)
        {
            var filter = new BsonDocument("name", collectionName);
            var options = new ListCollectionNamesOptions { Filter = filter };

            return Database.ListCollectionNames(options).Any();
        }

        public IMongoCollection<T> GetCollection<T>(string collectionName)
        {
            return Database.GetCollection<T>(collectionName);
        }
    }
}
