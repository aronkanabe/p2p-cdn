﻿using AutoMapper;
using p2p_cdn.api.Core.Models;
using p2p_cdn.api.Persistence.DAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver.GridFS;
using p2p_cdn.api.Core.Mappings;

namespace p2p_cdn.api.Persistence.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {

            CreateMap<TorrentFileDAL, TorrentFile>();
            CreateMap<TorrentFile, TorrentFileDAL>()
                .ForMember(torrentFileDAL => torrentFileDAL.Id, map => map.MapFrom(torrentFile => ObjectId.Parse(torrentFile.Id)))
                .ForMember(torrentFileDAL => torrentFileDAL.Content, map => map.MapFrom(torrentFile => torrentFile.Content));

            CreateMap<DataDAL, Data>()
                .ForMember(data => data.Id, map => map.MapFrom(dataDAL => dataDAL.Id.ToString()));
            CreateMap<Data, DataDAL>()
                .ForMember(dataDAL => dataDAL.Id, map => map.MapFrom(data => ObjectId.Parse(data.Id)));

            // CreateMap<GridFSFileInfo<ObjectId>, DataDAL>()
            //     .ForMember(dataDAL => dataDAL.Id, map => map.MapFrom(gridFsFileInfo => gridFsFileInfo.Id))
            //     .ForMember(dataDAL => dataDAL.OriginalName, map => map.MapFrom(gridFsFileInfo => gridFsFileInfo.Metadata["originalName"]))
            //     .ForMember(dataDAL => dataDAL.Guid, map => map.MapFrom(gridFsFileInfo => Guid.Parse(gridFsFileInfo.Filename)))
            //     .ForMember(dataDAL => dataDAL.Base64Content, map => map.MapFrom(gridFsFileInfo => gridFsFileInfo.))
        }
    }
}
