#!/bin/sh

dotnet test /p:CollectCoverage=true /p:CoverletOutputFormat=lcov /p:CoverletOutput=coverage.info --logger "junit;LogFilePath=report.xml"

dotnet reportgenerator --tool-path tools/dotnet-reportgenerator -reports:BuildReports/Coverage/report.xml -targetdir:BuildReports/Coverage -reporttypes:"HTML;HTMLSummary"