using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using FluentAssertions;
using Newtonsoft.Json;
using Xunit;

namespace p2p_tracker.Tests.IntegrationTests
{
    public class DataControllerTests : IntegrationTests
    {
        public DataControllerTests(ApiWebApplicationFactory factory) : base(factory)
        {
        }

        [Theory]
        [InlineData("/api/Data")]
        public async Task SmoketestShouldResultInOK(string endpoint)
        {
            var response = await client.GetAsync(endpoint);

            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Theory]
        [InlineData("emptyTestFile")]
        public async Task UploadCorrectData(string fileName)
        {
            HttpResponseMessage response;

            using(var file = File.Open($"IntegrationTests/TestFiles/{fileName}", FileMode.Open))
            using(var memoryStream = new MemoryStream())
            using(var form = new MultipartFormDataContent())
            {
                file.CopyTo(memoryStream);
                byte[] data = memoryStream.ToArray();
                ByteArrayContent byteArrayContent = new ByteArrayContent(data);
                byteArrayContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data");
                byteArrayContent.Headers.ContentDisposition.Name = "contentFile";
                byteArrayContent.Headers.ContentDisposition.FileName = fileName;
                byteArrayContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");

                form.Add(byteArrayContent, "contentFile", fileName);

                memoryStream.Dispose();
                
                response = await client.PostAsync("/api/Data", form);
            }

            response.EnsureSuccessStatusCode();

            string responseString = await response.Content.ReadAsStringAsync();
            
            Assert.NotEmpty(responseString);

            Dictionary<string, string> responseDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseString);
            
            Assert.Equal(fileName, responseDictionary["originalName"]);
            Assert.True(Guid.TryParse(responseDictionary["guid"], out _));

            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
        }
    }
}