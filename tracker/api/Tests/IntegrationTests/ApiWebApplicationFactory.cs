using System;
using System.Reflection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;

[assembly: WebApplicationFactoryContentRoot("p2p-tracker, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "./", "p2p-tracker.tests.dll", "1")]
namespace p2p_tracker.Tests.IntegrationTests
{
    public class ApiWebApplicationFactory: WebApplicationFactory<p2p_tracker.Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {

            builder.ConfigureTestServices(services =>
            {
            });
        }
    }
}