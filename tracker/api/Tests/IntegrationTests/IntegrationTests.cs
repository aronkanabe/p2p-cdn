using System;
using System.Net.Http;
using Xunit;

namespace p2p_tracker.Tests.IntegrationTests
{
    public abstract class IntegrationTests: IClassFixture<ApiWebApplicationFactory>
    {
        protected readonly ApiWebApplicationFactory factory;

        protected readonly HttpClient client;

        protected IntegrationTests(ApiWebApplicationFactory factory)
        {
            this.factory = factory;
            client = factory.CreateClient();
        }
    }
}