using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace p2p_tracker.Tests
{
    public static class Extensions
    {
        public static Task<T> GetAndDeserialize<T>(this HttpClient client, string requestUri)
        {
            return client.GetFromJsonAsync<T>(requestUri);
        }
    }
}