﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace p2p_cdn.api.Core.Models
{
    public class Data : BaseModel
    {
        public Data()
        {
            Guid = Guid.NewGuid();
            Content = Stream.Null;
        }

        public Data(string originalName, Guid guid, Stream content)
        {
            OriginalName = originalName;
            Guid = guid;
            Content = content;
        }

        public string OriginalName { get; set; }

        public Guid Guid { get; set; }

        public Stream Content { get; set; }
    }
}
