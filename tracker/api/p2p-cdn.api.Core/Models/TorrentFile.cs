﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace p2p_cdn.api.Core.Models
{
    public class TorrentFile : BaseModel
    {
        public Guid Guid { get; set; }

        public string OriginalName { get; set; }

        public Stream Content { get; set; }

        public IDictionary<string, string> Tags { get; set; }

        public TorrentFile()
        {
            OriginalName = "";
            Guid = Guid.NewGuid();
            Content = new MemoryStream();
            Tags = new Dictionary<string, string>();
        }

        protected bool Equals(TorrentFile other)
        {
            return base.Equals(other) && Guid.Equals(other.Guid) && OriginalName == other.OriginalName &&
                   Equals(Content, other.Content) && Equals(Tags, other.Tags);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(base.GetHashCode(), Guid, OriginalName, Content, Tags);
        }
    }
}