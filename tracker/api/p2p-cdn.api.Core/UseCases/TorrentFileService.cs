﻿using p2p_cdn.api.Core.Contracts;
using p2p_cdn.api.Core.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MonoTorrent;
using p2p_cdn.api.Core.Configurations;
using TorrentFile = p2p_cdn.api.Core.Models.TorrentFile;

namespace p2p_cdn.api.Core.UseCases
{
    public class TorrentFileService : ITorrentFileService
    {
        private readonly TorrentOptions torrentOptions;
        public ITorrentFileRepository TorrentRepository { get; set; }

        public TorrentFileService(ITorrentFileRepository torrentRepository, IOptions<TorrentOptions> torrentOptions)
        {
            this.torrentOptions = torrentOptions.Value;
            TorrentRepository = torrentRepository;
        }

        public async Task<IEnumerable<TorrentFile>> FindAllAsync(CancellationToken cancellationToken = default)
        {
            return await TorrentRepository.GetAllAsync(cancellationToken);
        }

        public async Task<TorrentFile> FindByIdAsync(Guid guid, CancellationToken cancellationToken)
        {
            return await TorrentRepository.GetByIdAsync(guid, cancellationToken);
        }

        public async Task<TorrentFile> CreateAsync(Data data, string id, CancellationToken cancellationToken = default)
        {
            await WriteDataToDisk(data, cancellationToken);
            var torrentFile = new TorrentFile
            {
                Id = id,
                Guid = data.Guid,
                OriginalName = data.OriginalName,
                // create torrent file with monotorrent
                Content = await CreateTorrentFile(data, cancellationToken)
            };

            var fileWrite = WriteStreamToFile(torrentFile.Content, Path.Combine(torrentOptions.TorrentFilePath, $"{torrentFile.Guid}.torrent") , cancellationToken);
            var dbWrite = TorrentRepository.CreateAsync(torrentFile, cancellationToken);
            Task.WaitAll(new[] { fileWrite, dbWrite }, cancellationToken);

            return torrentFile;
        }

        private string GetDataPath(Data data)
        {
            return Path.Combine(torrentOptions.DataPath, data.Guid.ToString());
        }

        private async Task WriteDataToDisk(Data data, CancellationToken cancellationToken)
        {
            var filePath = GetDataPath(data);
            await WriteStreamToFile(data.Content, filePath, cancellationToken);
        }

        private static async Task WriteStreamToFile(Stream stream, string filePath, CancellationToken cancellationToken)
        {
            Directory.CreateDirectory(Path.GetDirectoryName(filePath) ?? throw new InvalidOperationException("File name cant be null"));
            await using var fileStream = File.OpenWrite(filePath);
            stream.Position = 0;
            await stream.CopyToAsync(fileStream, cancellationToken);
            await fileStream.FlushAsync(cancellationToken);
            fileStream.Close();
        }

        private async Task<Stream> CreateTorrentFile(Data data, CancellationToken cancellationToken)
        {
            var torrentCreator = new TorrentCreator();
            var trackerList = torrentOptions.Trackers;
            torrentCreator.Announces.Add(trackerList.ToList());
            torrentCreator.CreatedBy = "p2p_cdn";
            var httpSeedsWithId = torrentOptions.GetrightHttpSeeds.Select(url => $"{url}{data.Guid}");
            torrentCreator.GetrightHttpSeeds.AddRange(httpSeedsWithId);

            // left here for future reference, if set to true peers can only join through the tracker
            // torrentCreator.Private = true;
            
            // can track the hashing of the file
            // torrentCreator.Hashed = delegate

            var torrentFileSource = new TorrentFileSource(GetDataPath(data));

            var torrentFileStream = new MemoryStream();
            await torrentCreator.CreateAsync(torrentFileSource, torrentFileStream, cancellationToken);

            return torrentFileStream;
        }

        public void Dispose()
        {
            TorrentRepository.Dispose();
        }
    }
}
