﻿using p2p_cdn.api.Core.Contracts;
using p2p_cdn.api.Core.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace p2p_cdn.api.Core.UseCases
{
    public class DataService : IDataService
    {
        private readonly IUnitOfWork unitOfWork;

        private readonly ITorrentFileService torrentFileService;

        public DataService(IUnitOfWork unitOfWork, ITorrentFileService torrentFileService)
        {
            this.unitOfWork = unitOfWork;
            this.torrentFileService = torrentFileService;

            // swapped the repository to use the same session as the rest of the unit of work
            torrentFileService.TorrentRepository = unitOfWork.TorrentFileRepository;
        }

        public void Dispose()
        {
            unitOfWork.Dispose();
            GC.SuppressFinalize(this);
        }

        public async Task<Data> InsertDataAsync(Data data, CancellationToken cancellationToken = default)
        {
            if (data == null) throw new ArgumentNullException(nameof(data));
            unitOfWork.StartTransaction();

            string objectId;

            try
            {
                data = await unitOfWork.DataRepository.CreateAsync(data, cancellationToken);
                objectId = data.Id;
            } catch (ArgumentNullException e)
            {
                await unitOfWork.RollRollBackAsync(cancellationToken);
                throw;
            }

            try
            {
                await torrentFileService.CreateAsync(data, objectId, cancellationToken);
            }
            catch (ArgumentNullException e)
            {
                await unitOfWork.RollRollBackAsync(cancellationToken);
                throw;
            }

            await unitOfWork.SaveAsync(cancellationToken);
            return data;
        }
        
        public async Task<byte[]> GetByteRangeAsync(Guid guid, int start, int end, CancellationToken cancellationToken = default)
        {
            if (start < 0) throw new ArgumentOutOfRangeException(nameof(start));
            if (end < 0) throw new ArgumentOutOfRangeException(nameof(end));

            var data = await unitOfWork.DataRepository.GetByteRangeAsync(guid, start, end, cancellationToken);
            if (data == null) throw new NullReferenceException(nameof(data));

            return data;
        }

        public async Task<Stream> GetBytesAsync(Guid guid, CancellationToken cancellationToken = default)
        {
            return (await unitOfWork.DataRepository.GetByIdAsync(guid.ToString(), cancellationToken)).Content;
        }
    }
}
