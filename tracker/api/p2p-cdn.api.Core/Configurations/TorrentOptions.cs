﻿using System;
using System.Collections.Generic;

namespace p2p_cdn.api.Core.Configurations
{
    public class TorrentOptions
    {
        public const string Torrent = "Torrent";

        public List<string> Trackers { get; set; }
        public string DataPath { get; set; }
        
        public string TorrentFilePath { get; set; }
        public List<string> GetrightHttpSeeds { get; set; }

        public TorrentOptions()
        {
            Trackers = new List<string>();
            DataPath = string.Empty;
            TorrentFilePath = string.Empty;
            GetrightHttpSeeds = new List<string>();
        }
    }
}