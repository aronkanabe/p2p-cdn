﻿namespace p2p_cdn.api.Core.Configurations
{
    public class MongoOptions
    {
        public const string Mongo = "Mongo";
        
        public string Url { get; set; } = string.Empty;
        public string DatabaseName { get; set; } = string.Empty;
    }
}