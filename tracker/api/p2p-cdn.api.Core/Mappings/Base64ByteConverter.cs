﻿using AutoMapper;

namespace p2p_cdn.api.Core.Mappings
{
    public class Base64ByteConverter : ITypeConverter<byte[], string>
    {
        public string Convert(byte[] source, string destination, ResolutionContext context)
        {
            return System.Convert.ToBase64String(source);
        }
    }
}
