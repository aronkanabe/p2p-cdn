﻿using System.IO;
using AutoMapper;

namespace p2p_cdn.api.Core.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<byte[], string>()
                .ConvertUsing<Base64ByteConverter>();
            CreateMap<string, Stream>()
                .ConvertUsing<InMemoryStreamConverter>();
            CreateMap<Stream, string>()
                .ConvertUsing<InMemoryStreamToStringConverter>();
            CreateMap<byte[], Stream>()
                .ConvertUsing<ByteToStreamConverter>();
            CreateMap<string, Stream>()
                .ConvertUsing<InMemoryStringToStreamConverter>();
            CreateMap<Stream, byte[]>()
                .ConvertUsing<StreamToByteConverter>();
        }
    }
}
