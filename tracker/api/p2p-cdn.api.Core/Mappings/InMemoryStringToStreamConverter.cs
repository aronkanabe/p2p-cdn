﻿using System.IO;
using System.Text;
using AutoMapper;

namespace p2p_cdn.api.Core.Mappings
{
    public class InMemoryStringToStreamConverter : ITypeConverter<string, Stream>
    {
        public Stream Convert(string source, Stream destination, ResolutionContext context)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(source));
        }
    }
}
