﻿using System.IO;
using System.Text;
using AutoMapper;

namespace p2p_cdn.api.Core.Mappings
{
    public class ByteToStreamConverter : ITypeConverter<byte[], Stream>
    {
        public Stream Convert(byte[] source, Stream destination, ResolutionContext context)
        {
            destination = new MemoryStream(source);
            return destination;
        }
    }
}
