﻿using System.IO;
using System.Text;
using AutoMapper;

namespace p2p_cdn.api.Core.Mappings
{
    public class StreamToByteConverter : ITypeConverter<Stream, byte[]>
    {
        public byte[] Convert(Stream source, byte[] destination, ResolutionContext context)
        {
            using var memoryStream = new MemoryStream();
            source.Position = 0;
            source.CopyTo(memoryStream);
            destination = memoryStream.ToArray();
            return destination;
        }
    }
}
