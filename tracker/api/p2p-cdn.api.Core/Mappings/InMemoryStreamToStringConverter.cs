﻿using System.IO;
using System.Text;
using AutoMapper;

namespace p2p_cdn.api.Core.Mappings
{
    public class InMemoryStreamToStringConverter : ITypeConverter<Stream, string>
    {
        public string Convert(Stream source, string destination, ResolutionContext context)
        {
            source.Position = 0;
            using var streamReader = new StreamReader(source, Encoding.UTF8);
            destination = streamReader.ReadToEnd();
            return destination;
        }
    }
}
