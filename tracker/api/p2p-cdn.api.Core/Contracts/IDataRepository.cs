﻿using p2p_cdn.api.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace p2p_cdn.api.Core.Contracts
{
    public interface IDataRepository: IDisposable
    {
        Task<Data> GetByIdAsync(string guid, CancellationToken cancellationToken = default);
        Task<Data> CreateAsync(Data data, CancellationToken cancellationToken = default);

        Task UpdateAsync(Data data, CancellationToken cancellationToken = default);

        Task DeleteAsync(string guid, CancellationToken cancellationToken = default);
        Task<byte[]> GetByteRangeAsync(Guid guid, int start, int end, CancellationToken cancellationToken = default);
    }
}