﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using p2p_cdn.api.Core.Models;

namespace p2p_cdn.api.Core.Contracts
{
    public interface IDataService: IDisposable
    {
        Task<Data> InsertDataAsync(Data data, CancellationToken cancellationToken = default);
        Task<byte[]> GetByteRangeAsync(Guid guid, int start, int end, CancellationToken cancellationToken = default);
        
        Task<Stream> GetBytesAsync(Guid guid, CancellationToken cancellationToken = default);
    }
}