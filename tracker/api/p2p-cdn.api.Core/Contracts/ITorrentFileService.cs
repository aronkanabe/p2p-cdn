﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using p2p_cdn.api.Core.Models;

namespace p2p_cdn.api.Core.Contracts
{
    public interface ITorrentFileService : IDisposable
    {
        ITorrentFileRepository TorrentRepository { get; set; }
        Task<IEnumerable<TorrentFile>> FindAllAsync(CancellationToken cancellationToken = default);
        Task<TorrentFile> FindByIdAsync(Guid guid, CancellationToken cancellationToken);
        Task<TorrentFile> CreateAsync(Data data, string id, CancellationToken cancellationToken = default);
    }
}