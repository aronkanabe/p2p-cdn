﻿using System;
using System.Threading;
using System.Threading.Tasks;
using p2p_cdn.api.Core.Models;

namespace p2p_cdn.api.Core.Contracts
{
    public interface ITorrentFileRepository: IBaseRepository<TorrentFile>
    {
        new Task<TorrentFile> GetByIdAsync(Guid guid, CancellationToken cancellationToken = default);
    }
}