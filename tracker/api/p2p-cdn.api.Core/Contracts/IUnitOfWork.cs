﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace p2p_cdn.api.Core.Contracts
{
    public interface IUnitOfWork: IDisposable
    {
        IDataRepository DataRepository { get; }
        ITorrentFileRepository TorrentFileRepository { get; }

        Task RollRollBackAsync(CancellationToken cancellationToken = default);
        Task SaveAsync(CancellationToken cancellationToken = default);

        void StartTransaction();
    }
}
