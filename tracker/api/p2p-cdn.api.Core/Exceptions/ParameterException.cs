﻿using System;

namespace p2p_cdn.api.Core.Exceptions;

[Serializable]
public class ParameterException : Exception
{
    public ParameterException() {}
    
    public ParameterException(string parameterName)
        : base($"Invalid parameter: {parameterName}")
    {
    }
}