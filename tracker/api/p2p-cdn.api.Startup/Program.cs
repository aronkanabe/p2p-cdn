using System;
using System.Linq;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using p2p_cdn.api.API;
using p2p_cdn.api.Core.Contracts;
using p2p_cdn.api.Core.UseCases;
using p2p_cdn.api.Persistence;
using p2p_cdn.api.Persistence.Repositories;
using Microsoft.Extensions.Configuration;
using p2p_cdn.api.API.Mappings;
using p2p_cdn.api.Core.Configurations;

namespace p2p_cdn.api.Startup
{
    class Program
    {
        static void Main(string[] args)
        {
            var apiMain = new APIMain(args);
            apiMain.Run((context, services) =>
            {
                services.Configure<MongoOptions>(context.Configuration.GetSection(MongoOptions.Mongo));
                services.Configure<TorrentOptions>(context.Configuration.GetSection(TorrentOptions.Torrent));
                services.AddSingleton<IMongoClient>(new MongoClient(context.Configuration.GetSection(MongoOptions.Mongo)
                    .Get<MongoOptions>().Url));
                services.AddScoped<IMongoDbContext, MongoDbContext>();
                var mapperConfig = new MapperConfiguration(mc =>
                {
                    mc.AddProfile(new Core.Mappings.MappingProfile());
                    mc.AddProfile(new Persistence.Mappings.MappingProfile());
                    mc.AddProfile(new MappingProfile());
                });

                var mapper = mapperConfig.CreateMapper();

                services.AddSingleton(mapper);

                services.AddScoped<ITorrentFileRepository, TorrentFileRepository>();
                services.AddScoped<IDataRepository, DataRepository>();

                services.AddScoped<IUnitOfWork, UnitOfWork>();
                services.AddScoped<ITorrentFileService, TorrentFileService>();
                services.AddScoped<IDataService, DataService>();
            });
        }
    }
}
