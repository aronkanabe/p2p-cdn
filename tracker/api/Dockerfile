FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src

COPY ["p2p-cdn.api.Core/p2p-cdn.api.Core.csproj", "p2p-cdn.api.Core/"]
RUN dotnet restore "p2p-cdn.api.Core/p2p-cdn.api.Core.csproj"

COPY ["p2p-cdn.api.API/p2p-cdn.api.API.csproj", "p2p-cdn.api.API/"]
RUN dotnet restore "p2p-cdn.api.API/p2p-cdn.api.API.csproj"

COPY ["p2p-cdn.api.Persistence/p2p-cdn.api.Persistence.csproj", "p2p-cdn.api.Persistence/"]
RUN dotnet restore "p2p-cdn.api.Persistence/p2p-cdn.api.Persistence.csproj"

COPY ["p2p-cdn.api.Startup/p2p-cdn.api.Startup.csproj", "p2p-cdn.api.Startup/"]
RUN dotnet restore "p2p-cdn.api.Startup/p2p-cdn.api.Startup.csproj"
COPY . .
WORKDIR "/src/p2p-cdn.api.Startup"
RUN dotnet build "p2p-cdn.api.Startup.csproj" --no-restore -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "p2p-cdn.api.Startup.csproj" --no-restore -r linux-musl-x64 -p:PublishSingleFile=true -c Release -o /app/publish

FROM mcr.microsoft.com/dotnet/runtime-deps:6.0-alpine as final
EXPOSE 5000
EXPOSE 5001
RUN addgroup -S trackergroup && \
    adduser -S trackeruser
RUN mkdir -p /data/torrentfiles &&\
    mkdir -p /data/files &&\
    chown -R trackeruser:trackergroup /data/torrentfiles &&\
    chown -R trackeruser:trackergroup /data/files
COPY --from=publish --chown=trackeruser:trackergroup /app/publish/appsettings.json appsettings.json
COPY --from=publish --chown=trackeruser:trackergroup /app/publish/p2p-cdn.api.Startup p2p-cdn.api.Startup
CMD ["./p2p-cdn.api.Startup"]

