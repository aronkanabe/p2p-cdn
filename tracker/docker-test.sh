#!/bin/sh
echo | dotnet --version
dotnet tool install dotnet-reportgenerator-globaltool --tool-path $path/tools
dotnet test $path --filter p2p_cdn.tracker.$type /p:CollectCoverage=true /p:CoverletOutputFormat=lcov /p:CoverletOutput=$type-coverage.info --logger "junit;LogFilePath=$path/BuildReports/$type-report.xml" --collect:"XPlat Code Coverage"
$path/tools/reportgenerator "-reports:$path/TestResults/*/coverage.cobertura.xml" "-targetdir:$path/BuildReports/Coverage" -reportTypes:"HTML;HTMLSummary;TextSummary"
cat $path/BuildReports/Coverage/Summary.txt
echo 'End Summary'